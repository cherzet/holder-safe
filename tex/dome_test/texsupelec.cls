% This document class provides a simple supelec for LaTeX users.
% It is based on article.cls and inherits most of the functionality
% that class.
% 
% Author: Clément Elvira, Copyright 2020. Released under the CeCILL license.

\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{texsupelec}[2020/09/04 - Simple for proposal]

% Load the Base Class
\LoadClassWithOptions{article}

% Begin Requirements
\RequirePackage{ifthen}
% \RequirePackage{palatino}
\RequirePackage{libertine}

\usepackage{graphicx}




\def\@supelectitle{\relax}
\newcommand{\supelectitle}[1]{\gdef\@supelectitle{#1}}

\def\@supelectuteur{\relax}
\newcommand{\supelectuteur}[1]{\gdef\@supelectuteur{#1}}

\def\@supelecporteur{\relax}
\newcommand{\supelecporteur}[1]{\gdef\@supelecporteur{#1}}

\def\@supelecwebsite{\relax}
\newcommand{\supelecwebsite}[1]{\gdef\@supelecwebsite{#1}}

\def\@supelecinter{\relax}
\newcommand{\supelecinter}[1]{\gdef\@supelecinter{#1}}

\def\@supeleckeywords{\relax}
\newcommand{\supeleckeywords}[1]{\gdef\@supeleckeywords{#1}}

\def\@supelecto{\relax}
\newcommand{\supelecto}[1]{\gdef\@supelecto{#1}}

\def\@supelecfrom{\relax}
\newcommand{\supelecfrom}[1]{\gdef\@supelecfrom{#1}}

\def\@supelecsubject{\relax}
\newcommand{\supelecsubject}[1]{\gdef\@supelecsubject{#1}}

\def\@supelecdate{\relax}
\newcommand{\supelecdate}[1]{\gdef\@supelecdate{#1}}

\def\@supeleclogo{\relax}
\newcommand{\logo}[1]{\gdef\@supeleclogo{\protect #1}}

\def\@letterheadaddress{\relax}
\newcommand{\lhaddress}[1]{\gdef\@letterheadaddress{#1}}




% Custom Document Formatting
\newcommand\decorativeline[1][1pt]{
	\par\noindent%
	\rule[0.5ex]{\linewidth}{#1}\par
}

% Set the Paper Size and margins
\RequirePackage{geometry}
\geometry{margin=1.0in}


% Create the Letterhead and To/From Block
\renewcommand{\maketitle}{\makesupelectitle}
\newcommand\makesupelectitle{
	\ifthenelse{\equal{\@supeleclogo}{\relax}}{}
	{ % Create With Logo
	\begin{minipage}[t]{1\columnwidth}%
		\begin{flushright}
			\vspace{-0.6in}
			\@supeleclogo
			\vspace{0.5in}
		\par\end{flushright}%
	\end{minipage}
	}
	{\begin{center}
	\LARGE\bf
	\ifthenelse{\equal{\@supelectitle}{\relax}}{}{\@supelectitle}
	\end{center}}
	% To, From, Subject Block
	\begin{description}
		\ifthenelse{\equal{\@supelecporteur}{\relax}}{}{\item [{Porteur:}] \@supelecporteur}
		\ifthenelse{\equal{\@supelectuteur}{\relax}}{}{\item [] \@supelectuteur}
		\ifthenelse{\equal{\@supelecinter}{\relax}}{}{\item [{Intervenants:}] \@supelecinter}
		\ifthenelse{\equal{\@supelecto}{\relax}}{}{\item [{To:}] \@supelecto}
		\ifthenelse{\equal{\@supelecfrom}{\relax}}{}{\item [{From:}] \@supelecfrom}
		\ifthenelse{\equal{\@supelecsubject}{\relax}}{}{\item [{Subject:}] \@supelecsubject}
		\ifthenelse{\equal{\@supelecdate}{\relax}}{}{\item [{Date:}] \@supelecdate}
		\ifthenelse{\equal{\@supeleckeywords}{\relax}}{}{\item [{Keywords:}] \@supeleckeywords}
	\end{description}
	\decorativeline\bigskip{}
}



\usepackage{url}
\usepackage{fancyhdr}
\usepackage{lastpage}
\pagestyle{fancy}

\renewcommand{\headrulewidth}{0pt}
\renewcommand{\footrulewidth}{0.4pt}
\lhead{}
\rhead{}
\lfoot{\scriptsize\ifthenelse{\equal{\@supelecwebsite}{\relax}}{}{\@supelecwebsite}}
\cfoot{}
\rfoot{Page~\thepage/\pageref{LastPage}}