%!TEX program = xelatex
%!TEX spellcheck = en_US
%!TEX output_directory = aux

\documentclass[letterpaper,11pt]{texsupelec} % Set the paper size (letterpaper, a4paper, etc) and font size (10pt, 11pt or 12pt)

% \usepackage[french]{babel}
\usepackage{parskip} % Adds spacing between paragraphs
\usepackage{amssymb,amsmath,mathtools}

\usepackage{booktabs}
\usepackage{kmath}

\setlength{\parindent}{15pt} % Indent paragraphs

\input{../common/headings}
\input{../common/com_notations}
\input{../common/def_macro}

% \mathtoolsset{showonlyrefs}

%------------------------------------------------------------------------------
%	MEMO INFORMATION
%------------------------------------------------------------------------------

\supelectitle{Implementing the dome test}
\supelectuteur{Clément Elvira}
\supelecwebsite{\url{https://c-elvira.github.io/}}

\supeleckeywords{}

\logo{\includegraphics[width=0.3\textwidth]{../common/logo.png}}

%----------------------------------------------------------------------------

% \newcommand{\pv}{\bfx}
\newcommand{\pvdim}{n}

\newcommand{\obsdim}{m}


\newcommand{\spherec}{\bfc}
\newcommand{\spherer}{R}
\newcommand{\halfnorm}{\bfn}
\newcommand{\halfnormnorm}[1]{}
% \newcommand{\halfnormnorm}[1]{\kvvbar{\halfnorm}_2^{#1}}
\newcommand{\halfseuil}{\delta}

\newcommand{\subgradient}{\bfg}

\begin{document}

\maketitle

%------------------------------------------------------------------------------
%	MEMO CONTENT
%------------------------------------------------------------------------------

\section{Contribution of the note}

\paragraph{Regions.}
Let
\begin{itemize}
	\item \(\calS\) denotes the safe sphere defined by
	      \begin{equation}
		      \calS \triangleq
		      \kset{
			      \dv\in\kR^\obsdim
		      }{
			      \kangle{\dv - \spherec} \leq \spherer
		      }
	      \end{equation}
	      where \(\spherec\in\kR^\obsdim\) is the center and \(\spherer\geq0\) is the radius.

	\item \(\calH\) denotes the half-space set defined by
	      \begin{equation}
		      \calH \triangleq
		      \kset{
			      \dv\in\kR^\obsdim
		      }{
			      \ktranspose{\halfnorm}\dv \leq \halfseuil
		      }
	      \end{equation}
	      where \(\halfnorm\geq0\) is a scalar and \(\halfnorm\in\kR^\obsdim\) the normal to the cone.
\end{itemize}

\paragraph{Considered problem.}
We consider finding
\begin{equation}
	m_+^* \triangleq
	\max_{\dv\in\calS\cap\calH} \ktranspose{\atom}\dv
	%  \quad\text{s.t.}\quad
	% \begin{cases}
	% \ktranspose{\kparen{\dv - \spherec}}\kparen{\dv - \spherec}
	% - \spherer^2 \leq 0 \\
	% %
	% \ktranspose{\halfnorm}\dv - \halfseuil \leq 0
\end{equation}
which is a quantity of interest to design the screening test.

% \paragraph{Hypotheses.}
We assume that the following hypotheses hold true throughout the note
\begin{enumerate}
	\item \(\atom \neq {\bf0}_\obsdim\)
	      \label{item:hyp atom not zero}
	\item \(\kvvbar{\halfnorm}_2=1\) (which implies \(\halfnorm \neq {\bf0}_\obsdim\))
	      \label{item:hyp:halfnorm unit norm}
	\item \(\delta \geq 0\)\footnote{\remCE{Is this hypothesis still used in the final version?}}
	      \label{item:hyp:nonnegative treshold}
	\item the dome is non-empty.
	      \label{item:hyp:halfnorm dome non-empty}
	      We admit (for now) this item is tantamount to have
	      \begin{equation}
		      \label{eq:item:hyp:halfnorm dome non-empty}
		      \frac{\ktranspose{\halfnorm}\spherec - \halfseuil}{\spherer} \in\kintervcc{-1}{1}
		      .
	      \end{equation}
\end{enumerate}


% \clearpage
% \paragraph{Main result.}
\begin{proposition}
	\label{prop:main result}
	Under hypotheses~\ref{item:hyp atom not zero}-\ref{item:hyp:halfnorm unit norm}-\ref{item:hyp:nonnegative treshold} and~\ref{item:hyp:halfnorm dome non-empty}, one has
	\begin{equation}
		\label{eq:prop:main result}
		\max_{\dv\in\calS\cap\calH} \ktranspose{\atom}\dv
		=
		\begin{cases}
			\ktranspose{\atom}\spherec + \spherer\kvvbar{\atom}_2
			 & \text{ if } \frac{\spherer}{\kvvbar{\atom}_2}\ktranspose{\halfnorm}\atom + \ktranspose{\halfnorm}\spherec \leq \delta
			\\
			\ktranspose{\atom}\spherec
			- \ktranspose{\atom}\halfnorm(\ktranspose{\halfnorm}\spherec - \delta)
			+ \sqrt{
				\kvvbar{\atom}_2^2 - (\ktranspose{\atom}\halfnorm)^2
			}
			\sqrt{
				\spherer^2\halfnormnorm{2} - (\ktranspose{\halfnorm}\spherec - \delta)^2
			}
			 & \text{ otherwise.}
		\end{cases}
	\end{equation}
\end{proposition}



\clearpage

\section[Proof of Proposition~\ref{prop:main result}]{Proof of \Cref{prop:main result}}

Consider the optimization problem
\begin{equation}
	\max_{\dv\in\kR^m} \atom\dv  \quad\text{s.t.}\quad
	\begin{cases}
		\ktranspose{\kparen{\dv - \spherec}}\kparen{\dv - \spherec}
		- \spherer^2 \leq 0 \\
		%
		\ktranspose{\halfnorm}\dv - \halfseuil \leq 0
	\end{cases}
\end{equation}
which is equivalent to
\begin{equation}
	- \min_{\dv\in\kR^m} - \atom\dv  \quad\text{s.t.}\quad
	\begin{cases}
		\ktranspose{\kparen{\dv - \spherec}}\kparen{\dv - \spherec}
		- \spherer^2 \leq 0 \\
		%
		\ktranspose{\halfnorm}\dv - \halfseuil \leq 0
		.
	\end{cases}
\end{equation}

\paragraph{Dual function.}
Let \(\mu_1,\mu_2\geq0\).
The lagrangian function associated to the minimization problem writes
\[
	L(\dv,\mu_1,\mu_2) =
	- \atom\dv
	+ \mu_1 \ktranspose{\kparen{\dv - \spherec}}\kparen{\dv - \spherec}
	- \mu_1\spherer^2
	+ \mu_2\ktranspose{\halfnorm}\dv
	- \mu_2\halfseuil
	.
\]
If \(\mu_1>0\), zeroing the gradient with respect to \(\dv\) leads to
\[
	-\atom + 2\mu_1\kparen{\dv_0 - \spherec} + \mu_2\halfnorm = {\bf0}_\obsdim
\]
or, equivalently
\begin{equation}
	\label{minimizer}
	\dv_0 = \spherec + \frac{1}{2\mu_1}\atom - \frac{\mu_2}{2\mu_1}\halfnorm.
\end{equation}
Hence the dual function (see \Cref{app:dual function} for additional calculation details)
\begin{equation}
	\label{eq:dual-function:V1}
	D(\mu_1,\mu_2) \triangleq
	\begin{cases}
		- \ktranspose{\atom}\spherec
		- \frac{1}{4\mu_1}\kvvbar{\atom}_2^2
		- \mu_1\spherer^2
		- \frac{\mu_2^2}{4\mu_1}\kvvbar{\halfnorm}_2^2
		+ \frac{\mu_2}{2\mu_1}\ktranspose{\atom}\halfnorm
		+ \mu_2\kparen{
			\ktranspose{\halfnorm}\spherec
			-
			\halfseuil
		}
		\quad & \text{if $\mu_1\neq0$}
		\\
		- \ktranspose{\atom}\spherec
		+ \mu_2\kparen{
			\ktranspose{\halfnorm}\spherec
			-
			\halfseuil
		}
		=
		-
		\mu_2\halfseuil
		\quad & \text{if $\mu_1=0$ and } \atom = \mu_2\halfnorm
		\\
		- \infty
		\quad & \text{otherwise.}
	\end{cases}
\end{equation}
Using the conventions ``\(x/0=+\infty\)'' and ``\(0/0=0\)'',~\eqref{eq:dual-function:V1} can be compactly rewritten as
\begin{equation}
	\label{eq:dual-function:V2}
	D(\mu_1,\mu_2)
	=
	- \ktranspose{\atom}\spherec
	-\frac{1}{4\mu_1}\kvvbar{\atom - \mu_2\halfnorm}_2^2
	- \mu_1\spherer^2
	+ \mu_2\kparen{
		\ktranspose{\halfnorm}\spherec - \halfseuil
	}
	.
\end{equation}
Since strong duality holds~\remCE{add ref}, we have
\begin{equation}
	\max_{\mu_1,\mu_2\geq0} D(\mu_1,\mu_2)
	\quad=\quad
	\min_{\dv\in\kR^m} - \atom\dv  \quad\text{s.t.}\quad
	\begin{cases}
		\ktranspose{\kparen{\dv - \spherec}}\kparen{\dv - \spherec}
		- \spherer^2 \leq 0 \\
		%
		\ktranspose{\halfnorm}\dv - \halfseuil \leq 0
		.
	\end{cases}
\end{equation}
We now exhibit the maximum in the left-hand-side of the former by distinguishing two cases, see \Cref{subsec:case noncolinear,subsec:case colinear}.

\include{case2}

\include{case1}




% \bibliographystyle{unsrtnat}
% \bibliography{bib}

\clearpage
\appendix

\section{Evaluating the dual function} \label{app:dual function}

We recall the definition of the dual function
\[
	L(\dv,\mu_1,\mu_2) =
	- \ktranspose{\atom}\dv
	+ \mu_1 \ktranspose{\kparen{\dv - \spherec}}\kparen{\dv - \spherec}
	- \mu_1\spherer^2
	+ \mu_2\ktranspose{\halfnorm}\dv - \mu_2\halfseuil
	.
\]
as well as the minimizer with respect to \(\dv\) whenever \(\mu_1\neq0\)
\[
	\dv_0 = \spherec + \frac{1}{2\mu_1}\atom - \frac{\mu_2}{2\mu_1}\halfnorm.
\]
Injecting \(\dv_0\) into \(L\) leads to
\begin{align*}
	L(\dv_0,\mu_1,\mu_2)
	\,=\, &
	- \ktranspose{\atom}\spherec - \frac{1}{2\mu_1}\kvvbar{\atom}_2^2 + \frac{\mu_2}{2\mu_1}\ktranspose{\atom}\halfnorm
	+ \frac{1}{4\mu_1}\kvvbar{\atom - \mu_2\halfnorm}_2^2 - \mu_1\spherer^2
	+ \mu_2\ktranspose{\halfnorm}\spherec
	+ \frac{\mu_2}{2\mu_1}\ktranspose{\halfnorm}\atom - \frac{\mu_2^2}{2\mu_1}\kvvbar{\halfnorm}_2^2 - \mu_2\halfseuil
	\\
	\,=\, &
	- \ktranspose{\atom}\spherec - \frac{1}{2\mu_1}\kvvbar{\atom}_2^2 + \frac{\mu_2}{\mu_1}\ktranspose{\atom}\halfnorm
	+ \frac{1}{4\mu_1}\kvvbar{\atom - \mu_2\halfnorm}_2^2 - \mu_1\spherer^2
	- \frac{\mu_2^2}{2\mu_1}\kvvbar{\halfnorm}_2^2
	+ \mu_2\ktranspose{\halfnorm}\spherec
	- \mu_2\halfseuil
	\\
	\,=\, &
	- \ktranspose{\atom}\spherec - \frac{1}{4\mu_1}\kvvbar{\atom}_2^2 + \frac{\mu_2}{2\mu_1}\ktranspose{\atom}\halfnorm
	- \mu_1\spherer^2
	- \frac{\mu_2^2}{4\mu_1}\kvvbar{\halfnorm}_2^2
	+ \mu_2\ktranspose{\halfnorm}\spherec
	- \mu_2\halfseuil
	\\
	\,=\, &
	- \ktranspose{\atom}\spherec
	- \frac{1}{4\mu_1}\kvvbar{\atom}_2^2
	- \mu_1\spherer^2
	- \frac{\mu_2^2}{4\mu_1}\kvvbar{\halfnorm}_2^2
	+ \frac{\mu_2}{2\mu_1}\ktranspose{\atom}\halfnorm
	+ \mu_2\ktranspose{\halfnorm}\spherec
	- \mu_2\halfseuil
	.
\end{align*}

\end{document}