# -*- coding: utf-8 -*-
import os, errno


def makedirs(foldername):
   """Create folder if it does not exists
   
   Parameters
   ----------
   foldername : str
      folder name
   
   Raises
   ------
   e : OSError
      different from "folder already exists"
   """

   try:
      os.makedirs(foldername)

   except OSError as e:
      if e.errno != errno.EEXIST:
         raise e