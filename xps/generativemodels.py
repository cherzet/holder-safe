# -*- coding: utf-8 -*-
import numpy as np


def generative_model(model, matA):
   """ Generate observation vecy
   
   Parameters
   ----------
   matA : np.ndarray
      Dictionary matrix
 		size [m, n]
   model : String
      generative model for y
   
   Returns
   -------
   vecy : np.ndarray
      Observation vector
      size [m,]
   
   Raises
   ------
   ValueError
      if parameter model has invalid value
   """

   m = matA.shape[0]
   n = matA.shape[1]

   # -- Case 1: vecy is uniformly distributed on the m-dimensional
   #            unit sphere
   if model == "unit-sphere":
      vecy  = np.random.randn(matA.shape[0])
      return vecy / np.linalg.norm(vecy, 2)

   # -- Case 2: vecy is given by a linear combination of a few columns
   #            of matA
   if "-sparse" in model:
      k = int(model.split("-")[0])

      nonzeroindex = np.random.permutation(n)[:k]

      vecx = np.zeros(n)
      vecx[nonzeroindex] = np.random.normal(0., 1., k)

      vecy = matA @ vecx

      return vecy / np.linalg.norm(vecy, 2)

   # -- Otherwise: invalid parameter
   raise ValueError("generative model should be \'unit-sphere\' or \'k-sparse\'")

