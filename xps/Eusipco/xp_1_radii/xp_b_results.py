# -*- coding: utf-8 -*-
import argparse, sys

import numpy as np

from src.lasso.ista import Ista
from src.lasso.fista import Fista
from src.utils.algparameters import AlgParameters
from src.utils.dictionaries import generate_dic

from setup import Setup
from callback import CallbackXp
import xps.utils as utils
from xps.generativemodels import generative_model


# -------------------------------------------
#           Loading experiment parameters 
# -------------------------------------------

   # Commands
parser=argparse.ArgumentParser()
parser.add_argument('--id', help='setup id', type=str, default=1)
parser.add_argument('--erase', help='restart xp', action="store_true")
args=parser.parse_args()

folder = f'results'
utils.makedirs(folder)

seed_filename    = f"{folder}/setup{args.id}_a_seeds.npz"
results_filename = f"{folder}/setup{args.id}_b_results.npz"

   # Load setup
setup = Setup(args.id)

   # Load seeds
out = np.load(seed_filename, allow_pickle=True)
mat_seed = out["mat_seed"]


# -------------------------------------------
#            Load existing results
# -------------------------------------------

try:
   if args.erase:
      raise FileNotFoundError

   load_results = np.load(results_filename, allow_pickle=True)
   mat_sum_radii    = load_results['mat_sum_radii']
   mat_count_radii  = load_results['mat_count_radii']
   t_done           = load_results['t']

except FileNotFoundError:
   # Everything goes well

   mat_sum_radii   = np.zeros(setup.dim_results)
   mat_count_radii = np.zeros(setup.dim_results)
   t_done = 0


# -------------------------------------------
#                  Experiment
# -------------------------------------------

print(f"Starting Benchmark with id {args.id}")
t = 0
nb_xp = setup.n_rep * setup.nb_dic * len(setup.list_ratio_lbd)
for rep in range(setup.n_rep):
   for i_dic in range(setup.nb_dic):
      for i_ratio, ratio in enumerate(setup.list_ratio_lbd):
         print(f"xp {t+1} / {nb_xp}")

         if t < t_done:
            t += 1
            continue

         callback = CallbackXp(setup)

         # --- set seed ---
         np.random.seed(mat_seed[i_dic, i_ratio, rep])


         # --- Data and parameters ---
         matA = generate_dic(
            setup.list_dic[i_dic],
            setup.m,
            setup.n,
            setup.normalize
         )

         vecy = generative_model(setup.y_model, matA)

         lbd_max = np.linalg.norm(matA.T @ vecy, np.inf)
         lbd     = setup.list_ratio_lbd[i_ratio] * lbd_max


         # --- Solve Lasso problems ---
         parameters = AlgParameters()

         parameters.iter_stop = np.inf
         parameters.gap_stop  = 10**(setup.log_gap_min)
         parameters.flop_stop = np.inf
         parameters.save_iter = False

         alg = eval(f"{setup.alg}(matA, vecy)")
         alg.solve(lbd, parameters, callback)

         mat_sum_radii[:, :, i_dic, i_ratio]   += callback.mat_sum_radii
         mat_count_radii[:, :, i_dic, i_ratio] += callback.mat_count_radii

         t += 1

         # --- Saving ---
         np.savez(results_filename,
            mat_sum_radii   = mat_sum_radii,
            mat_count_radii = mat_count_radii,
            t=t,
            allow_pickle = True,
         )


# -------------------------------------------
#              End of story
# -------------------------------------------