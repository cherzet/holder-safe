# -*- coding: utf-8 -*-
import numpy as np

from src.lasso.acallback import ACallback

from src.screening.gapsphere import GapSphereTest
from src.screening.gapdome import GapDomeTest
from src.screening.holderdome import HolderDomeTest


class CallbackXp(ACallback):
   """docstring for Callback"""
   def __init__(self, setup):
      super(CallbackXp, self).__init__()
      
      # Rules stuff
      self.list_rules = [
         eval(f"{rulename}()") for rulename in setup.list_screening_rules
      ]

      self.nb_rules   = len(self.list_rules)

      # Radius stuff
      sizes = (self.nb_rules, setup.gap_nbpoints)
      self.mat_sum_radii   = np.zeros(sizes)
      self.mat_count_radii = np.zeros(setup.gap_nbpoints)

      self.gap_interval_centers = np.logspace(
         setup.log_gap_min, 
         setup.log_gap_max, 
         num=setup.gap_nbpoints, 
         base=10.,
      )


   def call_impl(self, vecy, matA, lbd, datastruct):
      """Summary
      
      Args:
          datastruct (TYPE): Description
      """

      # --- 1. Find which gap is concerned
      gap = datastruct.gap

      # Evaluate distance from current gap to gap intervals
      dist = np.abs(self.gap_interval_centers - gap)

      # Find corresponding box
      i_gap = np.argmin(dist)


      # --- 2. Find which gap is concerned
      kwargs = {
         "y": vecy,
         "Ax": datastruct.Ax,
         "ytAx": datastruct.ytAx,
         "normAx2": datastruct.normAx2,
         "norm2res": datastruct.norm2res,
         "normy2": datastruct.normy2, 
         "norm2y_minus_u": datastruct.norm2y_minus_u,
         "Aty": datastruct.Aty,
         "gap": datastruct.gap,
         "pfunc": datastruct.pfunc,
         "At_gradf_x": - datastruct.neg_grad,
         "coef_dual_scaling": datastruct.coef_dual_scaling,
         "lbd_normx1": datastruct.lbd_normx1,
         "AtAx": datastruct.AtAx,
      }

      for i_rule, rule in enumerate(self.list_rules):
         radius = rule.eval_radius(lbd, **kwargs)
         self.mat_sum_radii[i_rule, i_gap] += radius

      self.mat_count_radii[i_gap] += 1