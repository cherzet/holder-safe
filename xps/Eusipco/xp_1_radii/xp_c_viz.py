# -*- coding: utf-8 -*-
from decimal import Decimal
import argparse

import numpy as np
import matplotlib.pyplot as plt

from src.screening.gapsphere import GapSphereTest
from src.screening.gapdome import GapDomeTest
from src.screening.holderdome import HolderDomeTest

from setup import Setup

import xps.utils as utils


# -------------------------------------------
#        Loading experiment parameters 
# -------------------------------------------

parser=argparse.ArgumentParser()
parser.add_argument('--id', help='setup id', type=str)
parser.add_argument('--noshow', help='show plots', action="store_true")
parser.add_argument('--save', help='save figure', action="store_true")
args=parser.parse_args()


# -------------------------------------------
#        Loading experiment results 
# -------------------------------------------

folder = f'results'

setup = Setup(args.id)

# ---- load ----
results_filename = f"{folder}/setup{setup.setup_id}_b_results.npz"

out = np.load(results_filename, allow_pickle=True)
mat_sum_radii = out['mat_sum_radii']
mat_count_radii = out['mat_count_radii']


list_rule_names = [
   "vanilla" if rulename == "None" else eval(f"{rulename}().name")
   for rulename in setup.list_screening_rules
   ]

gap_interval_centers = np.logspace(
   setup.log_gap_min, 
   setup.log_gap_max, 
   num=setup.gap_nbpoints, 
   base=10.,
)

# -------------------------
#       Plot  results
# -------------------------
list_colors = ["blue", "orange", "green", "red", "purple"]

f, ax = plt.subplots(setup.nb_ratio_lbd, setup.nb_dic, figsize=(16, 9))
# f.suptitle(f"{algname} with precision {setup.precision} as flop budget", fontsize=14)

for i_dic in range(setup.nb_dic):   

   for i_lbd in range(setup.nb_ratio_lbd):

      for i_rule in range(len(list_rule_names)):
         ax[i_lbd, i_dic].plot(
            gap_interval_centers, 
            mat_sum_radii[i_rule, :, i_dic, i_lbd] / mat_count_radii[i_rule, :, i_dic, i_lbd],
            linewidth=2.,
            label=list_rule_names[i_rule]
         )


      ax[i_lbd, i_dic].set_xscale('log')
      ax[i_lbd, i_dic].set_yscale('log')

      # ax[i_lbd, i_dic].set_xlim([10**(setup.log_gap_min), 10**(setup.log_gap_max)])
      # ax[i_lbd, i_dic].set_ylim([0, 105])
   

      if i_lbd == 0:
         ax[i_lbd, i_dic].set_title(f"{setup.list_dic[i_dic]} dictionary", fontsize=14)
         ax[i_lbd, i_dic].legend()

      if i_lbd == setup.nb_ratio_lbd-1:
         ax[i_lbd, i_dic].set_xlabel("Achieved duality gap", fontsize=12)

      if i_dic == 0:
         ax[i_lbd, i_dic].set_ylabel("Radius of the safe region", fontsize=12)

if args.save:
   utils.makedirs("figs")
   plt.savefig(f"figs/setup{args.id}.pdf", bbox_inches='tight')


if not args.noshow:
   plt.show()