# -*- coding: utf-8 -*-
import json

import numpy as np


class Setup(object):
   """docstring for Setup"""
   def __init__(self, setup_id):
      super(Setup, self).__init__()
      self.setup_id = setup_id
      
      with open('setups.json') as json_file:
         data = json.load(json_file)[f"setup{setup_id}"]

      # -- Extract data
      self.m = data["m"]
      self.n = data["n"]

      	# dictionaries
      self.list_dic = data["dictionaries"]

      	# Are columns normalized?
      self.normalize = data["normalize"]

      	# Generative model used to generate samples
      self.y_model = data["y_model"]

      	# Number of repetition of the xp
      self.n_rep = data["n_rep"]

      	# Ration lbd / lbd_max
      self.list_ratio_lbd = data["list_ratio_lbd"]

      	# list of algorithms to run
      self.alg = data["algorithm"]

      # self.gap_precision = data["gap_precision"]

      self.list_screening_rules = data["screening_rules"]

      self.log_gap_min  = data["log_gap_min"]
      self.log_gap_max  = data["log_gap_max"]
      self.gap_nbpoints = data["gap_nbpoints"]

      # -- Utils
      self.nb_dic       = len(self.list_dic)
      self.nb_ratio_lbd = len(self.list_ratio_lbd)
      self.nb_rules     = len(self.list_screening_rules)

      self.dim_seeds   = (self.nb_dic, self.nb_ratio_lbd, self.n_rep)
      self.dim_results = (self.nb_rules, self.gap_nbpoints, self.nb_dic, self.nb_ratio_lbd)