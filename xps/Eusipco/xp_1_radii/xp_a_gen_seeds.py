# -*- coding: utf-8 -*-
import argparse, sys
import numpy as np

from setup import Setup
import xps.utils as utils


# --------- Experiment preparations ---------

parser=argparse.ArgumentParser()
parser.add_argument('--id', help='setup id', type=str)
parser.add_argument('--erase', help='restart xp', action="store_true")
args=parser.parse_args()

folder = f'results'
utils.makedirs(folder)

seed_filename = f"{folder}/setup{args.id}_a_seeds.npz"

# ------------ Seeds generations ------------

print(f"\n\n --- Starting \"seed generations\" --- \n")
print(f"   with id {args.id}\n")

setup = Setup(args.id)

mat_seed = np.random.randint(0, 2**8, size=setup.dim_seeds)


# -------------- Saving seeds --------------

try:
   if args.erase:
      raise FileNotFoundError

   load_results = np.load(seed_filename, allow_pickle=True)

   if not args.erase:
      print(" --- experiment already exists -- exit\n\n")
      sys.exit(1)

except FileNotFoundError:
   # Everything goes well
   pass

np.savez(
   seed_filename,
   mat_seed     = mat_seed,
   allow_pickle = True
)


# -------------- End of story --------------
print(f"\n\n --- End --- \n\n")