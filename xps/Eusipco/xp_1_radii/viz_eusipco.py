# -*- coding: utf-8 -*-
from decimal import Decimal
import argparse

import numpy as np
import matplotlib.pyplot as plt

from src.screening.gapsphere import GapSphereTest
from src.screening.gapdome import GapDomeTest
from src.screening.holderdome import HolderDomeTest

from setup import Setup

import xps.utils as utils

# pip install matplotlib-label-lines
# Doc: https://github.com/cphyc/matplotlib-label-lines
try:
   from labellines import labelLine, labelLines
except ImportError:
   import pip
   try:
	   pip.main(['install', 'matplotlib-label-lines'])
   except e:
   	print("Exception occurs while installing package.")
   	print("Possible solutions")
   	print("  1. Run within a virtual environment")
   	print("  2. Add the '--user argument to the pip command above.")
   	print(e)
   	exit()
   from labellines import labelLine, labelLines



# -------------------------------------------
#        Loading experiment parameters 
# -------------------------------------------


parser=argparse.ArgumentParser()
# 1a
# Eusipco
parser.add_argument('--id', help='setup id', type=str, default='Eusipco2022')
parser.add_argument('--noshow', help='show plots', action="store_true")
parser.add_argument('--save', help='save figure', action="store_true")
args=parser.parse_args()


# -------------------------------------------
#        Loading experiment results 
# -------------------------------------------

folder = f'results'

setup = Setup(args.id)

# ---- load ----
results_filename = f"{folder}/setup{setup.setup_id}_b_results.npz"

out = np.load(results_filename, allow_pickle=True)
mat_sum_radii    = out['mat_sum_radii']
mat_count_radii  = out['mat_count_radii']

list_rule_names = [
   "vanilla" if rulename == "None" else eval(f"{rulename}().name")
   for rulename in setup.list_screening_rules
   ]

gap_interval_centers = np.logspace(
   setup.log_gap_min, 
   setup.log_gap_max, 
   num=setup.gap_nbpoints, 
   base=10.,
)

# -------------------------
#       Specif stuff
# -------------------------

list_index_dic_fig = [0, 1]
nb_dic_fig         = len(list_index_dic_fig) 
list_title_dic_fig = []
for i_dic in list_index_dic_fig:
   if setup.list_dic[i_dic] == "gaussian 0.":
      list_title_dic_fig.append("Gaussian")

   if setup.list_dic[i_dic] == "toeplitz":
      list_title_dic_fig.append("Toeplitz")


list_colors = [
   '#969696',
   '#525252',
   '#000000',
]

# -------------------------
#       Plot  results
# -------------------------

f, ax = plt.subplots(1, nb_dic_fig, figsize=(6, 2), sharex=True, sharey=True)
# f.suptitle(f"{algname} with precision {setup.precision} as flop budget", fontsize=14)

for i_dic_abs in range(nb_dic_fig):  

   for i_lbd in range(setup.nb_ratio_lbd):

      i_dic = list_index_dic_fig[i_dic_abs]

      radii_Holder = mat_sum_radii[2, :, i_dic, i_lbd] / mat_count_radii[2, :, i_dic, i_lbd]
      radii_gap    = mat_sum_radii[1, :, i_dic, i_lbd] / mat_count_radii[1, :, i_dic, i_lbd] 

      ax[i_dic_abs].plot(
         gap_interval_centers, 
         # mat_results[2, :, i_dic, i_lbd] / mat_results[1, :, i_dic, i_lbd],
         radii_Holder / radii_gap,
         linewidth=2.,
         color=list_colors[i_lbd],
         # label=f"$\\lambda/\\lambda_\\max=${setup.list_ratio_lbd[i_lbd]}",
         label=f"{setup.list_ratio_lbd[i_lbd]}",
      )

   # xvals = (.3, .5, .8)
   fs_label = 13
   if True: #not np.all(np.isnan( mat_results[2, :, i_dic, i_lbd])):
      if i_dic_abs == 0:
         labelLines(ax[i_dic_abs].get_lines(), xvals=(5e-2, 3.6e-2, 7.2e-3), zorder=2.5,
            fontsize=fs_label
         )
      if i_dic_abs == 1:
         labelLines(ax[i_dic_abs].get_lines(), xvals=(1.4e-2, 1e-2, 1.1e-3), zorder=2.5,
            fontsize=fs_label)



   ax[i_dic_abs].set_xscale('log')
   # ax[i_dic_abs].set_yscale('log')

   ax[i_dic_abs].set_xlim([10**(setup.log_gap_min), 2*10**(-1)])   

   ax[i_dic_abs].set_title(f"{list_title_dic_fig[i_dic_abs]}", fontsize=14)
   # ax[i_dic_abs].legend()

   ax[i_dic_abs].set_xlabel("Duality gap", fontsize=12)

   if i_dic_abs == 0:
      ax[i_dic_abs].set_ylabel("Ratio of radii", fontsize=12)

print("Remark: warning can be ignored as they corresponds to gap bins that do not contains points")

if args.save:
   utils.makedirs("figs")
   plt.savefig(f"figs/radii_eusipcoV2_setup{args.id}.pdf", bbox_inches='tight')

if not args.noshow:
   plt.show()
