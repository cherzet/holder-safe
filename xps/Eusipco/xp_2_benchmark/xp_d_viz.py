# -*- coding: utf-8 -*-
from decimal import Decimal
import argparse

import numpy as np
import matplotlib.pyplot as plt

from src.screening.gapsphere import GapSphereTest
from src.screening.gapdome import GapDomeTest
from src.screening.holderdome import HolderDomeTest

from setup import Setup
from process_data import process

import xps.utils as utils


# -------------------------------------------
#        Loading experiment parameters 
# -------------------------------------------

parser=argparse.ArgumentParser()
parser.add_argument('--id', help='setup id', type=str)
parser.add_argument('--noshow', help='show plots', action="store_true")
parser.add_argument('--save', help='save figure', action="store_true")
args=parser.parse_args()


# -------------------------------------------
#        Loading experiment results 
# -------------------------------------------

folder = f'results'

setup = Setup(args.id)
list_rule_names = [
   "vanilla" if rulename == "None" else eval(f"{rulename}().name")
   for rulename in setup.list_screening_rules
   ]
   # "vanilla", "GAP sphere", "GAP dome", "Le dome"
   # ]
dic_process = process(folder, setup)

vec_tau            = dic_process["vec_tau"]
results_rho        = dic_process["results_rho"]


# -------------------------
#       Plot  results
# -------------------------

for i_alg, algname in enumerate(setup.list_algs):

   f, ax = plt.subplots(setup.nb_ratio_lbd, setup.nb_dic, figsize=(16, 9))
   f.suptitle(f"{algname} with precision {setup.precision} as flop budget", fontsize=14)

   for i_dic in range(setup.nb_dic):   

      if np.all(results_rho[:, i_dic, :, :] == 0):
         continue

      for i_lbd in range(setup.nb_ratio_lbd):

         list_colors = ["blue", "orange", "green", "red", "purple"]
         for i_rule in range(len(list_rule_names)):
            ax[i_lbd, i_dic].plot(
               vec_tau, 
               100. * results_rho[i_rule, i_dic, i_lbd, i_alg],
               linewidth=2.,
               label=list_rule_names[i_rule]
            )


            ax[i_lbd, i_dic].set_xscale('log')

            ax[i_lbd, i_dic].set_xlim([1e-16, 1e1])
            ax[i_lbd, i_dic].set_ylim([0, 105])


            # if i_lbd == 0 :
               # ax[i_lbd, i_dic].set_title(f"$\\lambda / \\lambda_\\max$={setup.list_ratio_lbd[i_lbd]}")
               
            if i_lbd == 0:
               ax[i_lbd, i_dic].set_title(f"{setup.list_dic[i_dic]} dictionary", fontsize=14)
               ax[i_lbd, i_dic].legend()

            if i_lbd == setup.nb_ratio_lbd-1:
               ax[i_lbd, i_dic].set_xlabel("$\\tau$ (Dual gap)", fontsize=12)

            if i_dic == 0:
               ax[i_lbd, i_dic].set_ylabel("$\\rho_s(\\tau)$", fontsize=12)

      if args.save:
         utils.makedirs("figs")
         plt.savefig(f"figs/setup{args.id}_{algname}.pdf", bbox_inches='tight')

if not args.noshow:
   plt.show()
