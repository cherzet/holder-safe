# -*- coding: utf-8 -*-
import argparse, sys

import numpy as np

from src.lasso.fista import Fista
from src.screening.gapsphere import GapSphereTest
from src.screening.gapdome import GapDomeTest
from src.screening.holderdome import HolderDomeTest
from src.utils.algparameters import AlgParameters
from src.utils.dictionaries import generate_dic

from setup import Setup
import xps.utils as utils
from xps.generativemodels import generative_model


# -------------------------------------------
#           Loading experiment parameters 
# -------------------------------------------

   # Commands
parser=argparse.ArgumentParser()
parser.add_argument('--id', help='setup id', type=str, default=1)
parser.add_argument('--erase', help='restart xp', action="store_true")
args=parser.parse_args()

folder = f'results'
utils.makedirs(folder)

seed_filename    = f"{folder}/setup{args.id}_a_seeds.npz"
flop_filename    = f"{folder}/setup{args.id}_b_flops.npz"
results_filename = f"{folder}/setup{args.id}_c_results.npz"

   # Load setup
setup = Setup(args.id)

   # Load seeds
out = np.load(seed_filename, allow_pickle=True)
mat_seed = out["mat_seed"]

   # Load flop stuff
load_flops = np.load(flop_filename, allow_pickle=True)
mat_flops  = load_flops['mat_flops']


# -------------------------------------------
#            Load existing results
# -------------------------------------------

try:
   if args.erase:
      raise FileNotFoundError

   load_results = np.load(results_filename, allow_pickle=True)
   mat_results  = load_results['mat_results']

except FileNotFoundError:
   # Everything goes well

   mat_results = np.full(setup.dim_results, np.nan)


# -------------------------------------------
#                  Experiment
# -------------------------------------------

nb_xp = setup.nb_dic * setup.nb_ratio_lbd * setup.n_rep
t = 0
print(f"Starting Benchmark with id {args.id}")

list_rules = [
	None if rulename == "None" else eval(f"{rulename}()")
	for rulename in setup.list_screening_rules
	]

for i_dic in range(setup.nb_dic):
   for i_ratio, ratio in enumerate(setup.list_ratio_lbd):

      if not np.any(np.isnan(mat_results[:, i_dic, i_ratio, :, :])):
         t += setup.n_rep
         continue

      for rep in range(setup.n_rep):
         print(f"xp time {t+1} / {nb_xp}")

         # --- set seed ---
         np.random.seed(mat_seed[i_dic, i_ratio, rep])


         # --- Data and parameters ---
         matA = generate_dic(
            setup.list_dic[i_dic],
            setup.m,
            setup.n,
            setup.normalize
         )

         vecy = generative_model(setup.y_model, matA)

         lbd_max = np.linalg.norm(matA.T @ vecy, np.inf)
         lbd     = setup.list_ratio_lbd[i_ratio] * lbd_max


         # --- Solve Lasso problems ---
         for i_alg, algname in enumerate(setup.list_algs):
	         parameters = AlgParameters()

	         parameters.iter_stop = np.inf
	         parameters.gap_stop  = 1e-15
	         parameters.flop_stop = np.median(mat_flops[i_dic, i_ratio, :, i_alg])

	         for i_rule, rule in enumerate(list_rules):

	            if not np.isnan(mat_results[i_rule, i_dic, i_ratio, rep, i_alg]):
	               continue
	            
	            alg = eval(f"{algname}(matA, vecy, rule)")
	            alg.solve(lbd, parameters)

	            mat_results[i_rule, i_dic, i_ratio, rep, i_alg] = alg.gap


	         # --- Saving ---
	         np.savez(results_filename,
	            mat_results  = mat_results,
	            allow_pickle = True
	         )

         t += 1

# -------------------------------------------
#              End of story
# -------------------------------------------