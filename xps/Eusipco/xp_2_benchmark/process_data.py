# -*- coding: utf-8 -*-
import numpy as np

from setup import Setup


def process(folder, setup, log=True):
   
   # ---- load ----
   results_filename = f"{folder}/setup{setup.setup_id}_c_results.npz"

   out = np.load(results_filename, allow_pickle=True)
   mat_results = out['mat_results']


   # ---- processing ----
   vec_tau = np.logspace(-16, 0, num=200, base=10.)

   # Fonction rho(tau) %xp such that gap <= tau 
   results_rho = np.full(
      (setup.nb_rules, setup.nb_dic, setup.nb_ratio_lbd, setup.nb_algs, vec_tau.size),
      np.nan
   )

   for i in range(vec_tau.size):
      results_rho[:, :, :, :, i] = np.mean(mat_results <= vec_tau[i], axis=3)


   # ---- return ----
   return {
      "vec_tau": vec_tau,
      "results_rho": results_rho,
   }