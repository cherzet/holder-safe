# -*- coding: utf-8 -*-
from decimal import Decimal
import argparse

import numpy as np
import matplotlib.pyplot as plt

from src.screening.gapsphere import GapSphereTest
from src.screening.gapdome import GapDomeTest
from src.screening.holderdome import HolderDomeTest

from setup import Setup
from process_data import process

import xps.utils as utils


# -------------------------------------------
#        Loading experiment parameters 
# -------------------------------------------

parser=argparse.ArgumentParser()
parser.add_argument('--noshow', help='show plots', action="store_true")
parser.add_argument('--save', help='save figure', action="store_true")
args=parser.parse_args()


# -------------------------------------------
#        Loading experiment results 
# -------------------------------------------

folder = f'results'

setup = Setup('Eusipco2022')
list_rule_names = [
   "vanilla" if rulename == "None" else eval(f"{rulename}().name")
   for rulename in setup.list_screening_rules
   ]
   # "vanilla", "GAP sphere", "GAP dome", "Holder dome"
   # ]
dic_process = process(folder, setup)

vec_tau            = dic_process["vec_tau"]
results_rho        = dic_process["results_rho"]


# -------------------------
#       Specif stuff
# -------------------------

list_index_dic_fig  = [0, 2]
list_index_rule_fig = [1, 2, 3]
list_name_rule_fig  = ["", "GAP sphere", "GAP dome", "Hölder dome"]
nb_dic_fig          = len(list_index_dic_fig) 
nb_rule_fig         = len(list_index_dic_fig) 
list_title_dic_fig  = []
for i_dic in list_index_dic_fig:
   if setup.list_dic[i_dic] == "gaussian 0.":
      list_title_dic_fig.append("Gaussian")

   if setup.list_dic[i_dic] == "toeplitz":
      list_title_dic_fig.append("Toeplitz")


list_colors_fig    = ['#1f78b4', '#1f78b4', '#ff7f00']
list_linestyle_fig = ['--', '-', '-']

algname_fig = 'Fista'


# -------------------------
#       Plot  results
# -------------------------

for i_alg, algname in enumerate(setup.list_algs):

   if algname != algname_fig:
      continue

   f, ax = plt.subplots(setup.nb_ratio_lbd, nb_dic_fig, figsize=(6, 6), sharex=True, sharey=True)
   # f.suptitle(f"{algname} with precision {setup.precision} as flop budget", fontsize=14)

   for i_dic in range(nb_dic_fig):   

      if np.all(results_rho[:, i_dic, :, :] == 0):
         continue

      i_dic_fig = list_index_dic_fig[i_dic]

      for i_lbd in range(setup.nb_ratio_lbd):

         list_colors = ["blue", "orange", "green", "red", "purple"]

         idx = 0
         for i_rule in list_index_rule_fig:
            ax[i_lbd, i_dic].plot(
               vec_tau, 
               100. * results_rho[i_rule, i_dic_fig, i_lbd, i_alg],
               linewidth=2.,
               label=list_name_rule_fig[i_rule],
               color=list_colors_fig[idx],
               linestyle = list_linestyle_fig[idx],
            )
            idx += 1


            ax[i_lbd, i_dic].set_xscale('log')

            ax[i_lbd, i_dic].set_xlim([1e-15, 5e-2])
            ax[i_lbd, i_dic].set_ylim([0, 105])
               
            if i_lbd == 0:
               ax[i_lbd, i_dic].set_title(f"{list_title_dic_fig[i_dic]}", fontsize=14)

               if i_dic == 1:
               	ax[i_lbd, i_dic].legend()

            if i_lbd == setup.nb_ratio_lbd-1:
               ax[i_lbd, i_dic].set_xlabel("$\\tau$ (Dual gap)", fontsize=12)

            if i_dic == 0:
               ax[i_lbd, i_dic].set_ylabel("$\\rho(\\tau)$", fontsize=12)

      if args.save:
         utils.makedirs("figs")
         plt.savefig(f"figs/eusipco_benchmark_{algname}.pdf", bbox_inches='tight')


f.tight_layout()


if not args.noshow:
   plt.show()
