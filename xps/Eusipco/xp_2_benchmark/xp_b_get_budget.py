# -*- coding: utf-8 -*-
import argparse, sys

import numpy as np

from src.lasso.fista import Fista
from src.screening.gapsphere import GapSphereTest
from src.screening.gapdome import GapDomeTest
from src.screening.holderdome import HolderDomeTest
from src.utils.algparameters import AlgParameters
from src.utils.dictionaries import generate_dic

from setup import Setup
import xps.utils as utils
from xps.generativemodels import generative_model


# -------------------------------------------
#           Loading experiment parameters 
# -------------------------------------------

   # Commands
parser=argparse.ArgumentParser()
parser.add_argument('--id', help='setup id', type=str, default=1)
parser.add_argument('--erase', help='restart xp', action="store_true")
args=parser.parse_args()

folder = f'results'
utils.makedirs(folder)

seed_filename = f"{folder}/setup{args.id}_a_seeds.npz"
flop_filename = f"{folder}/setup{args.id}_b_flops.npz"

   # Load setup
setup = Setup(args.id)

   # Load seeds
out = np.load(seed_filename, allow_pickle=True)
mat_seed = out["mat_seed"]


# -------------------------------------------
#            Load existing results
# -------------------------------------------

try:
   if args.erase:
      raise FileNotFoundError

   load_results    = np.load(flop_filename, allow_pickle=True)
   mat_flops  = load_results['mat_flops']

except FileNotFoundError:
   # Everything goes well
   
   mat_flops = np.full(setup.dim_budget, np.nan)


# -------------------------------------------
#                  Experiment
# -------------------------------------------

print(f"\n\n --- Starting \"get budget\" with id {args.id}, precision {setup.precision} --- \n")

nb_xp = setup.nb_dic * setup.nb_ratio_lbd * setup.n_rep
t = 0

for i_dic in range(setup.nb_dic):
   for i_ratio, ratio in enumerate(setup.list_ratio_lbd):
      for rep in range(setup.n_rep):
         print(f"xp budget {t+1} / {nb_xp}")

         if not np.any(np.isnan(mat_flops[i_dic, i_ratio, rep, :])):
            # xp has already been done
            t += 1
            continue


         # --- set seed ---
         np.random.seed(mat_seed[i_dic, i_ratio, rep])


         # --- Data and parameters ---
         matA = generate_dic(
            setup.list_dic[i_dic],
            setup.m,
            setup.n,
            setup.normalize
         )

         vecy = generative_model(setup.y_model, matA)

         lbd_max = np.linalg.norm(matA.T @ vecy, np.inf)
         lbd     = setup.list_ratio_lbd[i_ratio] * lbd_max


         # --- Solve Lasso problems ---
         parameters = AlgParameters()

         parameters.iter_stop = np.inf
         parameters.gap_stop  = setup.precision
         parameters.flop_stop = np.inf

         # Algorithm loop
         for i_alg, algname in enumerate(setup.list_algs):

            rule = None if setup.rule_budget == "None" else f"{setup.rule_budget}()"

            alg = eval(f'{algname}(matA, vecy, {rule})')
            alg.solve(lbd, parameters)

            mat_flops[i_dic, i_ratio, rep, i_alg] = alg.flop.get_flops()


         # --- Saving ---
         np.savez(flop_filename,
            mat_flops  = mat_flops,
            allow_pickle    = True
         )

         t += 1

# -------------------------------------------
#              End of story
# -------------------------------------------
