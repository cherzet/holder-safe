# -*- coding: utf-8 -*-
import numpy as np


class Lasso(object):
   """Some useful tools to handle the Lasso problem
   
   Attributes
   ----------
   matA : TYPnp.ndarray
     dictionary matrix
     size [m, n]
   vecy : TYPnp.ndarray
     Observation vector
     size [m,]
   ratio_lbd : positive float
      ration lbd / lbdmax
   lbdmax : float, optional
      smallest regularization parameter such that the solution 
      to the Lasso problem is the zero vector
   lbd : float
   	value of the regularization parameter
   """

   def __init__(self, matA, vecy, ratio_lbd, lbdmax=None):
      """Summary
      
      Parameters
      ----------
      matA : TYPnp.ndarray
         dictionary matrix
         size [m, n]
      vecy : TYPnp.ndarray
         Observation vector
         size [m,]
      ratio_lbd : positive float
         ration lbd / lbdmax
      lbdmax : float, optional
         smallest regularization parameter such that the solution 
         to the Lasso problem is the zero vector
      """
      super(Lasso, self).__init__()
      self.matA = matA
      self.vecy = vecy
      self.ratio_lbd = ratio_lbd
      
      if lbdmax is None:
         self.lbdmax = np.linalg.norm(matA.T @ vecy, np.inf)
      else:
         self.lbdmax = lbdmax

      self.lbd = ratio_lbd * self.lbdmax


   def get_dual_scaling_factor(self, vecr):
      """Summary
      
      Parameters
      ----------
      vecr : TYPE
          Description
      
      Returns
      -------
      TYPE
          Description
      """

      return self.lbd / np.linalg.norm(self.matA.T @ vecr, np.inf)


   def dual_scaling(self, vecr):
      """ Dual scaling operator
      
      Parameters
      ----------
      vecr : np.ndarray
         vector to be dual scaled
         size [m,]
      
      Returns
      -------
      vecu : np.ndarray
         dual feasible vector obtained by dual scaling
         size [m,]
      """

      return self.get_dual_scaling_factor(vecr) * vecr


   def eval_primal_func(self, vecx):
      """Evaluate the value of the primal function at point vecx
      
      Parameters
      ----------
      vecx : np.ndarrat
         primal feasible point
         size [n,]
      
      Returns
      -------
      pval: float
         Value of the objectif function
      """

      vecr = self.vecy - self.matA @ vecx

      pval = .5 * np.linalg.norm(vecr, 2)**2
      pval += self.lbd * np.linalg.norm(vecx, 1)

      return pval


   def eval_duality_gap(self, vecx, vecu=None):
      """ Given primal and (optional dual feasible vector), return
      the duality gap associated to the Lasso problem

      Parameters
      ----------
      vecx : np.ndarray
         primal feasible vector
         size [n,]
      vecu : np.ndarrat, optional
         dual feasible vector
         (dual feasibility is not assumed)
         size [m, ]
      
      Returns
      -------
      gap : float
         duality gap
      """

      pval = self.eval_primal_func(vecx)

      if vecu is None:
         # residual error
         vecu = self.vecy - self.matA @ vecx

         # dual scaling
         vecu = self.dual_scaling(vecu)

      dval = .5 * np.linalg.norm(self.vecy, 2)**2
      dval -= .5 * np.linalg.norm(self.vecy - vecu, 2)**2

      return np.abs(pval - dval)