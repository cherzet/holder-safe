# -*- coding: utf-8 -*-

class Flop(object):
   """docstring for Flop
   
   Attributes
   ----------
   nb_add : int
      Number of addition performed
   nb_mult : int
      Number of multiplication
   """
   
   def __init__(self):
      """Summary
      """
      super(Flop, self).__init__()

      self.nb_mult       = 0
      self.nb_add        = 0
      self.nb_comparison = 0

   def __add__(self, flop2):
      """Summary
      
      Parameters
      ----------
      flop2 : Flop
      """

      self.nb_mult       += flop2.nb_mult
      self.nb_add        += flop2.nb_add
      self.nb_comparison += flop2.nb_comparison

      return self

   def get_flops(self):
      return self.nb_mult + self.nb_add

   def mult_operation(self, nb=1):
      self.nb_mult += nb

   def add_operation(self, nb=1):
      self.nb_add += nb
      
   def sqrt_operation(self, nb=1):
      pass

   def comparison_operation(self, nb=1):
      self.nb_comparison += nb


   # ---- Vector stuff ----
   def sum_vector(self, vec):
      self.nb_add += vec.size

   def inner_product(self, vec):
      self.nb_mult += vec.size
      self.nb_add += vec.size

   def norm_vector(self, vec):
      self.nb_mult += vec.size
      self.nb_add += vec.size

      self.sqrt_operation()

   def mult_vector_by_cst(self, vec):
      self.nb_mult += vec.size


   # ---- Matrix stuff ----
   def matrix_norm(self, mat):
      pass

   def matrix_vector_product(self, matA):
      self.nb_mult += matA.size
      self.nb_add  += matA.size