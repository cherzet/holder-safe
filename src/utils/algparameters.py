# -*- coding: utf-8 -*-
import numpy as np


class AlgParameters(object):
   """docstring for AlgParameters
   
   Attributes
   ----------
   bprint : bool
       Description
   flop_stop : TYPE
       Description
   gap_stop : float
       Description
   iter_stop : int
       Description
   lip : TYPE
       Description
   save_iter : bool
       Description
   xinit : TYPE
       Description
   """
   def __init__(self):
      super(AlgParameters, self).__init__()

      self.bprint = False

      self.iter_stop = 1000
      self.gap_stop  = 1e-3
      self.flop_stop = np.inf
      self.xinit = None
      self.lip   = None

      self.save_iter = True


   def print(self):

      cout = "Running algorithm with\n\n" \
         + " ** Stopping criterions\n**" \
         + f"\t - < {self.iter_stop} iterations\n" \
         + f"\t - < {self.gap_stop} as gap\n" \
         + f"\t - < {self.flop_stop} flop_stop\n" \
         + f"\n" \
         + " ** Parameters **\n" \
         + f"\t - < Lipchitz constant: {self.lip}\n" \
         + f"\t - < xinit: {self.xinit}"

      if self.bprint:
         print(cout)

   def check_parameters(self):

      if not isinstance(self.bprint, bool):
         raise ValueError("AlgParameters.bprint should be boolean")

      if not isinstance(int(self.iter_stop), int) or self.iter_stop < 0:
         raise ValueError("AlgParameters.iter_stop should be a positive integer")

      if not isinstance(self.gap_stop, float) or self.gap_stop <= 0:
         raise ValueError("AlgParameters.gap_stop should be a positive float")

      if not (isinstance(self.flop_stop, float) or isinstance(self.flop_stop, int)):
         raise ValueError("AlgParameters.flop_stop should be a positive integer / float")

      if self.xinit is not None and not isinstance(self.xinit, np.array):
         raise ValueError("AlgParameters.xinit should be (if not None) a numpy  array")
      elif len(self.xinit.size) != 1:
         raise ValueError("AlgParameters.xinit should be (if not None) a numpy array if size (n,)")   