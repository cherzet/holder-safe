# -*- coding: utf-8 -*-
from abc import ABC, abstractmethod

import numpy as np

from src.screening.ascreening import AScreening
from src.utils.flop import Flop


class ASphereTest(AScreening):
   """An abstract python class implementing Safe sphere test
   
   Attributes
   ----------
   required_quantities : list
      quantities required to perform the screening test
   """
   
   def __init__(self, testname):
      super(ASphereTest, self).__init__(testname)


   def apply_test_impl(self, lbd, **kwargs):
      """Apply screening test 
      
      Parameters
      ----------
      lbd : positive float
         regularization parameter
      **kwargs
         test specific parameters
      
      Return
      ------
      calI_screen : np.ndarray
         vector of boolean
         True if screening test passes and False otherwise
         size [n,]
      flop : Flop
         complexity of the screening test
      """
      # flop = Flop()

      # 1. Get dome parameters
      radius, Atc_over_coeff, coeff, flop = self.get_sphere_parameters(lbd, **kwargs)


      # 2. Implementing test
      #   "standard implementation"
      #   >> output_test = - kwargs["coef_dual_scaling"] * kwargs["At_gradf_x"] < (kwargs["lbd"] - radius)
      #
      #   "Efficient implementation"
      output_test = np.abs(Atc_over_coeff) < (lbd - radius) / coeff
      flop.mult_operation(1)
      flop.add_operation(1)
      flop.comparison_operation(Atc_over_coeff)

      # 3. Returns
      return output_test, flop



   @abstractmethod
   def get_sphere_parameters(self, lbd, **kwargs):
      """Apply screening test 
      
      Parameters
      ----------
      lbd : positive float
         regularization parameter
      **kwargs
         test specific parameters
      
      Returns
      -------
      radius : nonnegative float
         radius of the sphere parameter
      Atc_over_coeff : np.ndarray
         inner product between columns of dictionary and center of
         safe sphere divided by coeff
         size [n,]
      coeff : float
         inner product between normal vector and center of sphere
      flop : Flop
         complexity of the screening test
      """

      return radius, Atc_over_coeff, coeff, Flop()