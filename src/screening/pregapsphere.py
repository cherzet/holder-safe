# -*- coding: utf-8 -*-
from abc import ABC, abstractmethod

import numpy as np

from src.screening.asphere import ASphereTest
from src.utils.flop import Flop


class PreGapSphereTest(ASphereTest):
   """An abstract python class implementing Safe sphere test
   
   Attributes
   ----------
   required_quantities : list
      quantities required to perform the screening test
   """
   
   def __init__(self):
      super(PreGapSphereTest, self).__init__("Pre-GAP sphere")

      self.required_quantities = [
         "normy_minus_u", "coef_dual_scaling", "At_gradf_x", "Aty"
      ]


   def get_sphere_parameters(self, lbd, **kwargs):
      """Apply screening test 
      
      Parameters
      ----------
      lbd : positive float
         regularization parameter
      **kwargs
         test specific parameters
      
      Returns
      -------
      radius : nonnegative float
         radius of the sphere parameter
      Atc_over_coeff : np.ndarray
         inner product between columns of dictionary and center of
         safe sphere divided by coeff
         size [n,]
      coeff : float
         inner product between normal vector and center of sphere
      flop : Flop
         complexity of the screening test
      """

      flop = Flop()

      radius = .5 * kwargs["normy_minus_u"]
      flop.mult_operation(1)

      Atu = - kwargs["coef_dual_scaling"] * kwargs["At_gradf_x"]
      flop.mult_operation(Atu.size)

      Atc = .5 * (kwargs["Aty"] + Atu)
      flop.sum_vector(Atu)
      flop.mult_operation(Atu.size)

      return radius, Atc, 1., Flop()


   def eval_radius(self, lbd, **kwargs):
      """Evaluate the radius of the safe region
      
      Parameters
      ----------
      lbd : float
         Description
         regularization parameter
      **kwargs
           test specific parameters
      
      Returns
      -------
      radius : float
         Radius of the safe region
      
      """

      flop = Flop()

      # 1. Evaluating radius
      R = .5 * kwargs["normy_minus_u"]
      flop.mult_operation(1)

      return R