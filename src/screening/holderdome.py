# -*- coding: utf-8 -*-
import numpy as np

from src.screening.adome import ADomeTest
from src.utils.flop import Flop


class HolderDomeTest(ADomeTest):
   """A python class implementing the safe ``Holder dome'' test
   
   Attributes
   ----------
   required_quantities : list
      quantities required to perform the screening test
   """
   
   def __init__(self):
      super(HolderDomeTest, self).__init__("Holder dome")

      self.required_quantities = [
         "norm2y_minus_u",
         "lbd_normx1",
         "normAx2",
         "coef_dual_scaling",
         "ytAx",
         "At_gradf_x",
         "Aty",
         "AtAx",
      ]

   def get_dome_parameters(self, lbd, **kwargs):
      """Apply screening test 
      
      Parameters
      ----------
      lbd : positive float
         regularization parameter
      **kwargs
         test specific parameters
      
      Returns
      -------
      R : nonnegative float
         radius of the sphere parameter
      delta : nonnegative float
         treshold related to the cutting plane
      Atc : np.ndarray
         inner product between columns of dictionary and center of
         safe sphere
         size [n,]
      Atn : np.ndarray
         inner product between columns of dictionary and normal vector related
         of the cutting plane
         size [n,]
      ntc : float
         inner product between normal vector and center of sphere
      flop : Flop
         complexity of the screening test
      """

      flop = Flop()

      # 1. --- sphere stuff
      R =  .5 * np.sqrt(kwargs["norm2y_minus_u"])
      flop.mult_operation(1)
      flop.sqrt_operation(1)

      Atu = - kwargs["coef_dual_scaling"] * kwargs["At_gradf_x"]
      flop.mult_operation(Atu.size)

      Atc = .5 * (kwargs["Aty"] + Atu)
      flop.sum_vector(Atu)
      flop.mult_operation(1)

      if kwargs["lbd_normx1"] == 0:
         # cannot perform the test in that case
         return R, np.inf, Atc, Atc, -np.inf, flop

      # 2. --- dome stuffs

         # cutting plane coeff
      # normAx = np.linalg.norm(kwargs["Ax"], 2)
      # flop.norm_vector(kwargs["normy_minus_u"])
      normAx = np.sqrt(kwargs["normAx2"])
      flop.sqrt_operation(1)

      # vec2c = kwargs["y"] + kwargs["vecu"]
      # flop.sum_vector(vec2c)

      # ntc = vec2c.dot(kwargs["Ax"]) / (2. * normAx)
      # flop.inner_product(vec2c)
      # flop.mult_operation(2)

      ntc = .5 * ( \
         (1. + kwargs["coef_dual_scaling"]) * kwargs["ytAx"] \
         - kwargs["coef_dual_scaling"] * kwargs["normAx2"] \
      ) / normAx
      flop.add_operation(2)
      flop.mult_operation(4)

      delta = kwargs["lbd_normx1"] / normAx
      flop.mult_operation(1)

      Atn = kwargs["AtAx"] / normAx
      flop.mult_operation(Atn.size)

      return R, delta, Atc, Atn, ntc, flop


   def eval_radius(self, lbd, **kwargs):
      """Evaluate the radius of the safe region
      
      Parameters
      ----------
      lbd : float
         Description
         regularization parameter
      **kwargs
           test specific parameters
      
      Returns
      -------
      radius : float
         Radius of the safe region
      
      """

      flop = Flop()

      # --- 1. sphere stuff
      R =  .5 * np.sqrt(kwargs["norm2y_minus_u"])
      flop.mult_operation(1)
      flop.sqrt_operation(1)


      # If the hyperplane is degenerated, the dome becomes a sphere
      if kwargs["lbd_normx1"] == 0:
         # cannot perform the test in that case
         return R


      normAx = np.sqrt(kwargs["normAx2"])
      flop.sqrt_operation(1)


      # --- 2. Half-space threshold
      delta = kwargs["lbd_normx1"] / normAx


      # --- 3. Inner product between center of sphere and normal to half space
      ntc = .5 * ( \
         (1. + kwargs["coef_dual_scaling"]) * kwargs["ytAx"] \
         - kwargs["coef_dual_scaling"] * kwargs["normAx2"] \
      ) / normAx
      flop.add_operation(2)
      flop.mult_operation(4)


      # --- 4. Evaluate radius
         # Here, n is normalized to one
      psi = (delta - ntc) / R

      return self.eval_dome_radius(R, psi)