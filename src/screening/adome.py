# -*- coding: utf-8 -*-
from abc import ABC, abstractmethod

import numpy as np

from src.screening.ascreening import AScreening
from src.utils.flop import Flop


class ADomeTest(AScreening):
   """A python class implementing the dome test
   
   Attributes
   ----------
   required_quantities : list
      quantities required to perform the screening test
   """
   
   def __init__(self, testname):
      super(ADomeTest, self).__init__(testname)


   def apply_test_impl(self, lbd, **kwargs):
      """Apply screening test 
      
      Parameters
      ----------
      lbd : positive float
         regularization parameter
      **kwargs
         test specific parameters
      
      Return
      ------
      calI_screen : np.ndarray
         vector of boolean
         True if screening test passes and False otherwise
         size [n,]
      flop : Flop
         complexity of the screening test
      """
      # flop = Flop()

      # --- 1. Get dome parameters
      R, delta, Atc, Atn, ntc, flop = self.get_dome_parameters(lbd, **kwargs)

      if R is None:
         return np.zeros(kwargs["AtAx"].size, dtype=bool), flop

      # --- 2. Evaluate useful quantities
         # Phi
      phi = ntc - delta
      flop.add_operation(1)

         # sqrt coeff 2
      sqrtcoeff2 = R**2 - phi**2
      flop.add_operation(1)
      flop.mult_operation(2)


      # --- 3. Performing the test : testing max_u <a, u> < lambda

      vec_bound1 = np.zeros(Atc.size)
      lhs_dome = lbd - 1e-15
      flop.add_operation(1)

         # Evaluate indices
      ind_bools = Atn > - phi / R
      flop.comparison_operation(Atn.size)
      flop.mult_operation(1)

      countnonzero = np.count_nonzero(ind_bools)

         # Case 1
      vec_bound1[np.invert(ind_bools)] = Atc[np.invert(ind_bools)] + R
      flop.add_operation(ind_bools.size - countnonzero)

         # Case 2
         # clip is to handle the machine precision error
         # (-1 e-17 was observed)
      vec_bound1[ind_bools] = Atc[ind_bools] - phi * Atn[ind_bools] \
         + np.sqrt(
            np.clip(1. - Atn[ind_bools]**2, 0, np.inf) * sqrtcoeff2
         )
      flop.add_operation(2*countnonzero)
      flop.mult_operation(3 * countnonzero)
      flop.sqrt_operation(countnonzero)

         # Test (extra addition of numerical error management)
      output_test = vec_bound1 < lhs_dome
      flop.comparison_operation(vec_bound1.size)

      if not np.any(output_test):
         return output_test, flop


      # --- 4. Performing the test : testing max_u <-a, u> < lambda

         # Testing only entries that passes the first tests to save ressources
      indices_to_be_tested = np.where(output_test)[0]
      vec_bound2 = np.zeros(indices_to_be_tested.size)

         # Evaluate indices
      ind_bools = Atn[indices_to_be_tested] < phi / R
      flop.comparison_operation(ind_bools.size)
      flop.mult_operation(1)

      countnonzero = np.count_nonzero(ind_bools)

         # Case 1
      vec_bound2[np.invert(ind_bools)] = - Atc[indices_to_be_tested[np.invert(ind_bools)]] + R
      flop.add_operation(ind_bools.size - countnonzero)

         # Case 2
      vec_bound2[ind_bools] = - Atc[indices_to_be_tested[ind_bools]] \
         + phi * Atn[indices_to_be_tested[ind_bools]] \
         + np.sqrt(
               np.clip(1. - Atn[indices_to_be_tested[ind_bools]]**2, 0, np.inf) * sqrtcoeff2
            )
      flop.add_operation(2 * countnonzero)
      flop.mult_operation(3 * countnonzero)
      flop.sqrt_operation(countnonzero)

         # Test
      output_test[indices_to_be_tested] = vec_bound2 < lhs_dome
      flop.comparison_operation(vec_bound2.size)


      # --- 5. Returns
      return output_test, flop


   def eval_dome_radius(self, R, psi):
      """Summary
      
      Parameters
      ----------
      R : TYPE
          Description
      psi : TYPE
          Description
      """

      if psi >= -1 and psi <= 0:
         return R * np.sqrt(1 - psi**2)

      else: 
         return R


   @abstractmethod
   def get_dome_parameters(self, lbd, **kwargs):
      """Apply screening test 
      
      Parameters
      ----------
      lbd : positive float
         regularization parameter
      **kwargs
         test specific parameters
      
      Returns
      -------
      R : nonnegative float
         radius of the sphere parameter
      delta : nonnegative float
         treshold related to the cutting plane
      Atc : np.ndarray
         inner product between columns of dictionary and center of
         safe sphere
         size [n,]
      Atn : np.ndarray
         inner product between columns of dictionary and normal vector related
         of the cutting plane
         size [n,]
      ntc : float
         inner product between normal vector and center of sphere
      flop : Flop
         complexity of the screening test
      """

      return R, delta, Atc, Atn, ntc, Flop()