# -*- coding: utf-8 -*-
import numpy as np

from src.screening.adome import ADomeTest
from src.utils.flop import Flop


class GapDomeTest(ADomeTest):
   """A python class implementing the safe GAP dome test
   
   Attributes
   ----------
   required_quantities : list
      quantities required to perform the screening test
   """
   
   def __init__(self):
      super(GapDomeTest, self).__init__("GAP dome")

      self.required_quantities = [
         "norm2y_minus_u",
         "pfunc",
         "normy2",
         "coef_dual_scaling",
         "norm2res",
         "gap",
         "At_gradf_x",  
         "Aty",
        ]


   def get_dome_parameters(self, lbd, **kwargs):
      """Apply screening test 
      
      Parameters
      ----------
      lbd : positive float
         regularization parameter
      **kwargs
         test specific parameters
      
      Returns
      -------
      R : nonnegative float
         radius of the sphere parameter
      delta : nonnegative float
         treshold related to the cutting plane
      Atc : np.ndarray
         inner product between columns of dictionary and center of
         safe sphere
         size [n,]
      Atn : np.ndarray
         inner product between columns of dictionary and normal vector related
         of the cutting plane
         size [n,]
      ntc : float
         inner product between normal vector and center of sphere
      flop : Flop
         complexity of the screening test
      """

      flop = Flop()

         # sphere radius
      R =  .5 * np.sqrt(kwargs["norm2y_minus_u"])
      flop.mult_operation(1)
      flop.sqrt_operation(1)

      if R < 1e-5 or kwargs["pfunc"] > .5 * kwargs["normy2"]:
         return None, None, None, None, None, flop

         # cutting plane coeff
      # ntc = vec_n.dot(vecc)
      # ntc = (kwargs["normy2"] - np.linalg.norm(kwargs["vecu"], 2)**2) / (4 * R)
      # flop.norm_vector(kwargs["vecu"])
      # flop.add_operation(1)
      # flop.mult_operation(3)

      ntc = (kwargs["normy2"] - kwargs["coef_dual_scaling"]**2 * kwargs["norm2res"]) / (4 * R)
      flop.add_operation(1)
      flop.mult_operation(3)

      delta = kwargs["gap"] / R - R + ntc #vec_n.dot(vecc)
      flop.add_operation(2)
      flop.mult_operation(1)

      Atu = - kwargs["coef_dual_scaling"] * kwargs["At_gradf_x"]
      flop.mult_operation(Atu.size)

      Atc = .5 * (kwargs["Aty"] + Atu)
      flop.sum_vector(Atu)
      flop.mult_operation(Atu.size)

      Atn = .5 * (kwargs["Aty"] - Atu) / R
      flop.sum_vector(Atu)
      flop.mult_operation(Atu.size + 1)

      return R, delta, Atc, Atn, ntc, flop


   def eval_radius(self, lbd, **kwargs):
      """Evaluate the radius of the safe region
      
      Parameters
      ----------
      lbd : float
         Description
         regularization parameter
      **kwargs
           test specific parameters
      
      Returns
      -------
      radius : float
         Radius of the safe region
      
      """

      flop = Flop()

      # --- 1. sphere stuff
      R =  .5 * np.sqrt(kwargs["norm2y_minus_u"])
      flop.mult_operation(1)
      flop.sqrt_operation(1)


      # If the hyperplane is degenerated, the dome becomes a sphere
      if kwargs["lbd_normx1"] == 0:
         # cannot perform the test in that case
         return R


      # --- 2. Inner product between center of sphere and normal to half space
      ntc = (kwargs["normy2"] - kwargs["coef_dual_scaling"]**2 * kwargs["norm2res"]) / (4 * R)
      flop.add_operation(1)
      flop.mult_operation(3)


      # --- 3. Half-space threshold
      delta = kwargs["gap"] / R - R + ntc #vec_n.dot(vecc)
      flop.add_operation(2)
      flop.mult_operation(1)


      # --- 4. Evaluate radius
         # Here, n is normalized to one
      psi = (delta - ntc) / R

      return self.eval_dome_radius(R, psi)