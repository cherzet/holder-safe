# -*- coding: utf-8 -*-
from abc import ABC, abstractmethod

import numpy as np

from src.utils.flop import Flop

class AScreening(ABC):
   """ Abstract class for implementing screening test
 
   Attributes
   ----------
   name : str
      Name of the screening test
   required_quantities : list
      quantities required to perform the screening test
   check_params : bool
      set to true if to check all required parameters
      may impact performances
      default is false
   """

   def __init__(self, testname):
      """Constructor
      
      Parameters
      ----------
      testname : str
         Name of the screening test
      
      Raises
      ------
      ValueError
         if test name is not a string
      """
      super(AScreening, self).__init__()

      if not isinstance(testname, str):
         raise ValueError("testname should be a string containing the name of the screening test")

      self.name = testname

      self.required_quantities = []
      self.check_params = False


   def apply_test(self, lbd, **kwargs):
      """Apply screening test 
      
      Parameters
      ----------
      lbd : positive float
         regularization parameter
      **kwargs
         test specific parameters
      
      Returns
      -------
      calI_screen : np.ndarray
         vector of boolean
         True if screening test passes and False otherwise
         size [n,]
      """

      if self.check_params:
         self.check_parameters(**kwargs)

      return self.apply_test_impl(lbd, **kwargs)


   @abstractmethod
   def apply_test_impl(self, lbd, **kwargs):
      """Apply screening test 
      
      Parameters
      ----------
      lbd : positive float
         regularization parameter
      **kwargs
         test specific parameters
      
      Returns
      -------
      output_test : np.ndarray
         vector of boolean
         True if screening test passes and False otherwise
         size [n,]
      flop : Flop
         complexity of the screening test
      """

      return np.zeros(0), Flop()


   @abstractmethod
   def eval_radius(self, lbd, **kwargs):
      """Evaluate the radius of the safe region
      
      Parameters
      ----------
      lbd : float
         Description
         regularization parameter
      **kwargs
           test specific parameters
      
      Returns
      -------
      radius : float
         Radius of the safe region
      
      """
      
      return 0


   def check_parameters(self, **kwargs):
      """Check if all parameters are available to perform the test
      
      Parameters
      ----------
      **kwargs
         Test parameters
      
      Raises
      ------
      ValueError
         if one screening quantities is not available
      """

      for key in self.required_quantities:
         if key not in kwargs:
            raise ValueError(f"missing argument: \"{key}\"")
