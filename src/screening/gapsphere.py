# -*- coding: utf-8 -*-
import numpy as np

from src.screening.ascreening import AScreening
from src.utils.flop import Flop


class GapSphereTest(AScreening):
   """A python class implementing the safe GAP sphere test
   
   Attributes
   ----------
   required_quantities : list
      quantities required to perform the screening test
   check_params : bool
      set to true if to check all required parameters
      may impact performances
      default is false
   """
   
   def __init__(self):
      super(GapSphereTest, self).__init__("GAP Sphere")

      self.required_quantities = ["gap", "At_gradf_x", "coef_dual_scaling"]


   def apply_test_impl(self, lbd, **kwargs):
      """Apply screening test 
      
      Parameters
      ----------
      lbd : positive float
         regularization parameter
      **kwargs
         test specific parameters
      
      Return
      ------
      calI_screen : np.ndarray
         vector of boolean
         True if screening test passes and False otherwise
         size [n,]
      flop : Flop
      	complexity of the screening test
      """
      flop = Flop()

      # 1. Evaluating radius
      radius = np.sqrt(2. * kwargs["gap"])
      flop.mult_operation(1)
      flop.sqrt_operation(1)

      # 2. Implementing test
      #   "standard implementation"
      #   >> output_test = - kwargs["coef_dual_scaling"] * kwargs["At_gradf_x"] < (kwargs["lbd"] - radius)
      #
      #   "Efficient implementation"
      output_test = np.abs(kwargs["At_gradf_x"]) < (lbd - radius) / kwargs["coef_dual_scaling"]
      flop.mult_operation(1)
      flop.add_operation(1)
      flop.comparison_operation(kwargs["At_gradf_x"].size)

      # 3. Returns
      return output_test, flop


   def eval_radius(self, lbd, **kwargs):
      """Evaluate the radius of the safe region
      
      Parameters
      ----------
      lbd : float
         Description
         regularization parameter
      **kwargs
           test specific parameters
      
      Returns
      -------
      radius : float
         Radius of the safe region
      
      """

      flop = Flop()

      # 1. Evaluating radius
      R = np.sqrt(2. * kwargs["gap"])
      flop.mult_operation(1)
      flop.sqrt_operation(1)

      return R