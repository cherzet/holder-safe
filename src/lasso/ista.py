# -*- coding: utf-8 -*-
from abc import ABC, abstractmethod

import numpy as np

from src.lasso.alassosolver import ALassoSolver


class Ista(ALassoSolver):

   def __init__(self, matA, vecy, screening=None):
      """Summary
      
      Parameters
      ----------
      matA : TYPE
          Description
      vecy : TYPE
          Description
      screening : None, optional
          Description
      """
      super(Ista, self).__init__(matA, vecy, screening, "ISTA")

      # --- extra algorithmic quantities ---
      self.lip = np.linalg.norm(matA, 2)**2
      self.flop_init.matrix_norm(matA)
      self.flop_init.mult_operation(1)


   def solver_impl(self, lbd, vecx, datastruct):
      """ Perform one iteration of the ISTA algorithm
      
      Parameters
      ----------
      lbd : positive float
         regularization parameter
      vecx : np.ndarray
         current iterate
         size [n,]
      datastruct : src.lasso.structiteration.StructLassoIteration
         data structure containing all required quantities
         to perform one iteration

      
      Returns
      -------
      vecx : np.ndarray
         new iterate
      """

      lip = min(float(vecx.size), self.lip)

      vecx += datastruct.neg_grad / lip
      self.flop.mult_vector_by_cst(datastruct.neg_grad)
      self.flop.sum_vector(datastruct.neg_grad)

      # Repeard at each iteration -- could be avoided
      lbd_over_lip = lbd / lip
      self.flop.mult_operation(1)

      vecx = np.sign(vecx) * np.maximum(np.abs(vecx) - lbd_over_lip, 0)
      self.flop.sum_vector(vecx)                     # vecx - lbd / lip
      self.flop.mult_vector_by_cst(vecx)             # sign * max()
      self.flop.comparison_operation(3 * vecx.size)  # sign, max and abs

      return vecx