# -*- coding: utf-8 -*-
from abc import ABC, abstractmethod

import numpy as np

from src.lasso.structiteration import StructLassoIteration
from src.lasso.acallback import ACallback
from src.screening.ascreening import AScreening
from src.utils.flop import Flop
from src.utils.algparameters import AlgParameters


class ALassoSolver(object):
   """Abstract class for solving the Lasso problem
  
   Attributes
   ----------
   active_entries : np.array
      tensor of integer contained index of
      entries not set (yet) to zero
      size (n') with n'<= n, the size of the problem
   dual_func : float
      Value of the dual function
   gap : float
      Value of the duality gap
   primal_func : float
      Value of the primal function
   problem : src.problem.Aproblem
      problem to be solved
   screening : AScreening
      Instance of a screening class to perform the test
   solvername : str
      name of the solver
      ("no name" by default)
   vecu_hat : np.array
      Estimated solution of the dual problem
      size (n)
   vecx_hat : np.array
      Estimated solution of the primal problem
      size (m)
   """
   
   def __init__(self, matA, vecy, screening=None, solvername="Solver name"):
      """Summary
      
      Parameters
      ----------
      matA : np.ndarray
         considered dictionary
         size [m, n]
      vecy : np.ndarray
         observation vector
         size [m,]
      screening : src.screening.ascreening, optional
         Class that implements the screening test
         to be performed
         default is None (no screening)
      solvername : str, optional
         String containing the name of the solver
         Default is "Solver name"
      
      Raises
      ------
      ValueError
          if arguments are wrong type
      """

      # --------- check argument ---------
      if not isinstance(matA, np.ndarray) or len(matA.shape) != 2:
         raise ValueError(f"matA should be a mxn matrix")

      if not isinstance(vecy, np.ndarray) or len(vecy.shape) != 1:
         raise ValueError(f"vecy should be a mx1 matrix")

      if matA.shape[0] != vecy.size:
         raise ValueError("number of rows of matA should match the size of vecy")

      if not isinstance(solvername, str):
         raise ValueError(f"solvername should be a string")

      if screening is not None and not isinstance(screening, AScreening):
         classname = "src.screening.ascreening.AScreening"
         raise ValueError(f"screening has to inherit from {classname}")

      #
      if screening is not None:
         colnorms = np.linalg.norm(matA, axis=0)
         # As it is just an assessment, complexity is not evaluated here

         if np.max(colnorms) > 1 + 1e-10:
            raise ValueError("Columns of dictionary are not unit-norm") 


      # --------- Save attributes ---------

         # 1. Related to optimization problem and method
      self.m = matA.shape[0]
      self.n = matA.shape[1]
      self.solvername = solvername

         # 2. Save pointers
      self.vecy = vecy
      self.matA = matA

         # 3. algorithmic quantities
      self.flop_init = Flop()

      self.Aty = matA.T @ vecy
      self.flop_init.matrix_vector_product(matA)

      self.normy  = np.linalg.norm(vecy, 2)
      self.flop_init.norm_vector(vecy)

      self.normy2 = self.normy**2
      self.flop_init.mult_operation(1)

         # 4. Screening
      self.screening = screening


   def solve(self, lbd, algoparams, callback=None):
      """Solve th optimization problem up to a stopping criterion
      
      Parameters
      ----------
      lbd : positive float
         regularization parameter
      algoparams : TYPE
         Description
      callback : ACallback
         callback function to be called at each
         iteration
      
      Returns
      -------
      TYPE
          Description
      """

      if callback is not None:
         if not isinstance(callback, ACallback):
            raise ValueError("callback should be an instance of ACallback")


      self.flop = Flop()

      # --- 1. Initialization
      if algoparams.xinit is None:
         vecx = np.zeros(self.n)
      else:
         vecx = np.copy(algoparams.xinit)
      
         # Iteration number
      self.t = 0

      self.list_pfunc = []
      self.list_dfunc = []

         # --- Active entries
      ind_active = np.arange(self.n)

         # --- Data structure
      datastruct = StructLassoIteration()
      datastruct.vecy   = self.vecy
      datastruct.Aty    = self.Aty
      datastruct.normy  = self.normy
      datastruct.normy2 = self.normy2

      # --- 2. Iteration loop
      while True:

         # --- 2.a Evaluate main quantities
            # Regressor
         datastruct.Ax = self.matA[:, ind_active] @ vecx[ind_active]
         self.flop.matrix_vector_product(self.matA[:, ind_active])

            # Residual error
         datastruct.res = self.vecy - datastruct.Ax
         self.flop.sum_vector(self.vecy)

            # Negative gradient
         datastruct.AtAx = self.matA[:, ind_active].T @ datastruct.Ax
         self.flop.matrix_vector_product(self.matA[:, ind_active])

         datastruct.neg_grad = datastruct.Aty - datastruct.AtAx
         self.flop.sum_vector(datastruct.Aty)

            # Dual feasible point
         datastruct.coef_dual_scaling = \
            lbd / np.linalg.norm(datastruct.neg_grad, np.inf)
         self.flop.mult_operation(1)
         self.flop.comparison_operation(datastruct.neg_grad.size)

         # datastruct.vecu = datastruct.coef_dual_scaling * datastruct.res
         # self.flop.mult_vector_by_cst(datastruct.vecu)

            # Primal function
         datastruct.ytAx = self.vecy.dot(datastruct.Ax)
         self.flop.inner_product(self.vecy)

         datastruct.normAx2 = datastruct.Ax.dot(datastruct.Ax)
         self.flop.inner_product(datastruct.Ax)

         # datastruct.normres2 = datastruct.res.dot(datastruct.res)
         # self.flop.add_operation(datastruct.res.size)
         # self.flop.mult_operation(datastruct.res.size)

         datastruct.lbd_normx1 = lbd * np.linalg.norm(vecx[ind_active], 1)
         self.flop.sum_vector(vecx[ind_active])
         self.flop.mult_operation(1)

         # pfunc = .5 * datastruct.normres2 + lbd * datastruct.normx1
         # self.flop.mult_operation(2)
         # self.flop.add_operation(1)

         datastruct.norm2res = (datastruct.normy2 + datastruct.normAx2) - 2 * datastruct.ytAx
         self.flop.mult_operation(1)
         self.flop.add_operation(2)

         pfunc = .5 * datastruct.norm2res + datastruct.lbd_normx1
         self.flop.mult_operation(1)
         self.flop.add_operation(1)

         if algoparams.save_iter:
            self.list_pfunc.append(pfunc)
         datastruct.pfunc = pfunc

            # Dual function
         # datastruct.y_minus_u = self.vecy - datastruct.vecu
         # self.flop.sum_vector(datastruct.vecu)

         datastruct.norm2y_minus_u = (1 - datastruct.coef_dual_scaling)**2 * self.normy2
         self.flop.add_operation(1)
         self.flop.mult_operation(2)

         datastruct.norm2y_minus_u += datastruct.ytAx * \
            2 * (1 - datastruct.coef_dual_scaling) * datastruct.coef_dual_scaling
         self.flop.add_operation(2)
         self.flop.mult_operation(3)

         datastruct.norm2y_minus_u +=  datastruct.coef_dual_scaling**2 * datastruct.normAx2
         self.flop.add_operation(1)
         self.flop.mult_operation(2)

         # datastruct.normy_minus_u = np.linalg.norm(datastruct.y_minus_u, 2)
         # self.flop.norm_vector(datastruct.vecu)
         
         dfunc = .5 * (datastruct.normy2 - datastruct.norm2y_minus_u)
         self.flop.mult_operation(1)
         self.flop.add_operation(1)

         if algoparams.save_iter:
            self.list_dfunc.append(dfunc)
         datastruct.dfunc = dfunc

            # Duality gap (absolute values are optional)
         self.gap = pfunc - dfunc
         datastruct.gap = self.gap
         self.flop.add_operation(1)
         self.flop.comparison_operation(1)         

         # --- 2.b Callback
         if callback is not None:
            callback.call(self.vecy, self.matA, lbd, datastruct)

         # --- 2.c Stopping criterion
         crit_gap   = self.gap <  algoparams.gap_stop
         crit_it    = self.t   >= algoparams.iter_stop
         crit_flops = self.flop.get_flops() >= algoparams.flop_stop

         if crit_gap or crit_flops or crit_it:
            break

         # --- 2.d Screening
         if self.screening is not None:
               # Prepare stuff
            kwargs = {
               "y": self.vecy,
               "Ax": datastruct.Ax,
               "ytAx": datastruct.ytAx,
               "normAx2": datastruct.normAx2,
               "norm2res": datastruct.norm2res,
               # "vecu": datastruct.vecu, 
               # "y_minus_u": datastruct.y_minus_u, 
               "normy2": datastruct.normy2, 
               "norm2y_minus_u": datastruct.norm2y_minus_u,
               "Aty": datastruct.Aty,
               "gap": datastruct.gap,
               "pfunc": datastruct.pfunc,
               "At_gradf_x": - datastruct.neg_grad,
               "coef_dual_scaling": datastruct.coef_dual_scaling,
               "lbd_normx1": datastruct.lbd_normx1,
               "AtAx": datastruct.AtAx,
            }

               # Perform screening test
            output_test, flop = self.screening.apply_test(lbd, **kwargs)

               # Add flop
            self.flop = self.flop + flop

               # Apply results
            if np.any(output_test):
               # Set entries to zero
               vecx[ind_active[output_test]] = 0.

               # Reduce active set
               ind_active = ind_active[np.invert(output_test)]

               # Update data structure
               datastruct.Aty      = datastruct.Aty[np.invert(output_test)]
               datastruct.AtAx     = datastruct.AtAx[np.invert(output_test)]
               datastruct.neg_grad = datastruct.neg_grad[np.invert(output_test)]

         # End screening ---

         # --- 2.d Iteration update
         vecx[ind_active] = self.solver_impl(lbd, vecx[ind_active], datastruct)

         self.t += 1

      # end main loop ---

      # --- 3. return
      return vecx


   @abstractmethod
   def solver_impl(self, lbd, vecx, datastruct):
      """Summary
      
      Parameters
      ----------
      lbd : positive float
         regularization parameter
      vecx : np.ndarray
         current iterate
         size [n,]
      datastruct : src.lasso.structiteration.StructLassoIteration
         data structure containing all required quantities
         to perform one iteration

      Returns
      -------
      vecx : np.ndarray
         new iterate
      """

      return vecx
