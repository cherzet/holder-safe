# -*- coding: utf-8 -*-


class StructLassoIteration(object): 
   """Summary

      Ax : np.ndarray
         regression vector
         size [m,]
      neg_grad : np.ndarray
         negative gradient of cost funct at vecx
         size [n,]
      normx1 : nonnegative float
         value of the l1-norm evaluated at vecx

   """
    
   __slots__ = (
      "vecy",
   	"normy",
   	"normy2",
   	"Aty",
      "Ax",
      "AtAx",
      "ytAx",
      "normAx2",
      "neg_grad", 
      "lbd_normx1", 
      "res",
      "norm2res",
      "coef_dual_scaling",
      # "vecu",
      "normres2",
      # "y_minus_u",
      "norm2y_minus_u",
      "gap",
      "pfunc",
      "dfunc",
   )