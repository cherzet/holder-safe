# -*- coding: utf-8 -*-
from abc import ABC, abstractmethod

from src.lasso.structiteration import StructLassoIteration


class ACallback(object):
   """docstring for ACallback"""
   def __init__(self):
      super(ACallback, self).__init__()
      

   def call(self, vecy, matA, lbd, datastruct):
      
      if not isinstance(datastruct, StructLassoIteration):
         raise ValueError("")

      self.call_impl(vecy, matA, lbd, datastruct)


   @abstractmethod
   def call_impl(self, vecy, matA, lbd, datastruct):
      pass