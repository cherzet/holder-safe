# -*- coding: utf-8 -*-
from abc import ABC, abstractmethod

import numpy as np

from src.lasso.alassosolver import ALassoSolver


class Fista(ALassoSolver):

   def __init__(self, matA, vecy, screening=None):
      """Summary
      
      Parameters
      ----------
      matA : TYPE
          Description
      vecy : TYPE
          Description
      screening : None, optional
          Description
      """
      super(Fista, self).__init__(matA, vecy, screening, "FISTA")

      # --- extra algorithmic quantities ---
      self.lip = np.linalg.norm(matA, 2)**2
      self.flop_init.matrix_norm(matA)
      self.flop_init.mult_operation(1)


   def solver_impl(self, lbd, vecx, datastruct):
      """ Perform one iteration of the FISTA algorithm
      
      Parameters
      ----------
      lbd : positive float
         regularization parameter
      vecx : np.ndarray
         current iterate
         size [n,]
      datastruct : src.lasso.structiteration.StructLassoIteration
         data structure containing all required quantities
         to perform one iteration

      Returns
      -------
      vecx : np.ndarray
         new iterate
      """

      lip = min(float(vecx.size), self.lip)

      # 1. set fista parameters
      if self.t == 0:
         self.beta = 1.
         self.coeff = 0
         self.vecz        = np.copy(vecx)
         self.neg_gradf_z = np.zeros(vecx.size)

      if self.vecz.size != vecx.size:
         self.beta = 1.
         self.vecz = np.copy(vecx)
         self.neg_gradf_z = datastruct.neg_grad

      else:
      	self.neg_gradf_z += (1 + self.coeff) * datastruct.neg_grad
      	self.flop.mult_vector_by_cst(datastruct.neg_grad)
      	self.flop.add_operation(1)
      	self.flop.sum_vector(datastruct.neg_grad)

      vecx_buf = np.copy(vecx)

      # 2. Standard update
      vecx = self.vecz + self.neg_gradf_z / lip
      self.flop.mult_vector_by_cst(datastruct.Aty)
      self.flop.sum_vector(datastruct.Aty)

      # Repeard at each iteration -- could be avoided
      lbd_over_lip = lbd / lip
      self.flop.mult_operation(1)

      vecx = np.sign(vecx) * np.maximum(np.abs(vecx) - lbd_over_lip, 0)
      self.flop.sum_vector(vecx)                   # vecx - lbd / lip
      self.flop.mult_vector_by_cst(vecx)           # sign * max()
      self.flop.comparison_operation(3 * vecx.size)  # sign, max and abs

      # Fista stuff
      betabuf = float(self.beta)
      self.beta = (1. + np.sqrt(1. + 4 * (self.beta**2))) / 2.

      # -- Coefficient update
      self.coeff = (betabuf - 1.) / self.beta
      self.flop.add_operation(1)
      self.flop.mult_operation(1)

      # -- Update vecz
      self.vecz = vecx + self.coeff * (vecx - vecx_buf)
      self.flop.sum_vector(vecx)
      self.flop.sum_vector(vecx)
      self.flop.mult_vector_by_cst(vecx)

      # -- partial update neg_grad vecz 
      #    (the (1 + coeff) * vecx part is mussing)
      self.neg_gradf_z = - self.coeff * datastruct.neg_grad
      self.flop.mult_vector_by_cst(datastruct.neg_grad)

      # -- return
      return vecx