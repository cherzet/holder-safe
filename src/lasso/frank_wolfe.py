# -*- coding: utf-8 -*-
from abc import ABC, abstractmethod

import numpy as np

from src.lasso.alassosolver import ALassoSolver


class FrankWolfe(ALassoSolver):

   def __init__(self, matA, vecy, screening=None):
      """Summary
      
      Parameters
      ----------
      matA : TYPE
          Description
      vecy : TYPE
          Description
      screening : None, optional
          Description
      """
      super(FrankWolfe, self).__init__(matA, vecy, screening, "Frank-Wolfe")


   def solver_impl(self, lbd, vecx, datastruct):
      """Perform one iteration of the ISTA algorithm
      
      Parameters
      ----------
      lbd : positive float
         regularization parameter
      vecx : np.ndarray
         current iterate
         size [n,]
      datastruct : src.lasso.structiteration.StructLassoIteration
         data structure containing all required quantities
         to perform one iteration
      
      Returns
      -------
      vecx : np.ndarray
         new iterate
      """

      alpha = self.normy2 / (2. * lbd)
      self.flop.mult_operation(2)

      # --- 1. Find direction most correlated with residual
      inew = np.argmax(np.abs(datastruct.neg_grad))
      self.flop.comparison_operation(2*datastruct.neg_grad.size) # argmax+abs

      # --- 2. Create descent direction 
      dnew = np.zeros(self.n)
      Adnew = np.zeros(self.m)
      
      dnew[inew] = np.sign(datastruct.neg_grad[inew]) * alpha
      self.flop.mult_operation(1)

      Adnew = dnew[inew] * self.matA[:, inew]
      self.flop.mult_vector_by_cst(Adnew)
      
      # --- 3. Compute weight : 
      #        find pi in [0, 1] such pi vecx + (1-pi)d minimizes
      #        the cost function
      bfq = datastruct.Ax - Adnew # matA @ (x_star - dnew)
      self.flop.sum_vector(datastruct.Ax)

      pi = (bfq.dot(self.vecy - Adnew) + lbd * alpha - datastruct.lbd_normx1) \
      	/ (np.linalg.norm(bfq, 2)**2)


      pi = np.clip(pi, 0., 1.)
      self.flop.comparison_operation(2)

      # --- 4. Update estimated solution
      vecx = pi * vecx + (1. - pi) * dnew
      self.flop.mult_vector_by_cst(vecx)
      self.flop.mult_vector_by_cst(vecx)
      self.flop.mult_operation(1)

      # --- 5. Return
      return vecx
