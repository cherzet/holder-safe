# -*- coding: utf-8 -*-
import unittest

import numpy as np
from src.utils.dictionaries import generate_dic

from src.lasso.frank_wolfe import FrankWolfe
from src.utils.algparameters import AlgParameters

class TestIsta(unittest.TestCase):

   def test_vanilla_works(self):
      """ Run GAP test at "optimality"
         check if nonzero entry does no pass the test
      """

      # Lasso problem
      m = 20
      n = 50
      
      matA = generate_dic("gaussian", m, n)
      vecy = np.random.randn(m)
      vecy /= np.linalg.norm(vecy)

      lbdmax = np.linalg.norm(matA.T @ vecy, np.inf)
      lbd = .8 * lbdmax

      alg = FrankWolfe(matA, vecy)

      parameters = AlgParameters()

      alg.solve(lbd, parameters)

      # print(alg.gap)

      # import matplotlib.pyplot as plt
      # plt.plot(np.array(alg.list_pfunc))
      # plt.show()

if __name__ == '__main__':
    unittest.main()