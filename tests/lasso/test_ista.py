# -*- coding: utf-8 -*-
import unittest

import numpy as np
from src.utils.dictionaries import generate_dic

from src.lasso.ista import Ista
from src.screening.gapsphere import GapSphereTest
from src.screening.gapdome import GapDomeTest
from src.screening.holderdome import HolderDomeTest
from src.utils.algparameters import AlgParameters

class TestIsta(unittest.TestCase):

   def test_vanilla_works(self):
      """ Run GAP test at "optimality"
         check if nonzero entry does no pass the test
      """

      # Lasso problem
      m = 20
      n = 50
      
      np.random.seed(2401)
      matA = generate_dic("gaussian", m, n)

      vecy = np.random.randn(m)
      vecy /= np.linalg.norm(vecy, 2)

      lbdmax = np.linalg.norm(matA.T @ vecy, np.inf)
      lbd = .8 * lbdmax

      alg = Ista(matA, vecy)

      parameters = AlgParameters()

      alg.solve(lbd, parameters)

      self.assertTrue(alg.gap < parameters.gap_stop)


   def test_with_test(self):
      """ Run GAP test at "optimality"
         check if nonzero entry does no pass the test
      """

      # Lasso problem
      m = 20
      n = 50

      list_problematic_seed = [2401, 3503, 256985135]
      for seed in list_problematic_seed:
	      np.random.seed(seed)
	      matA = generate_dic("gaussian", m, n)

	      for j in range(n):
	      	matA[:, j] /= np.linalg.norm(matA[:, j], 2)

	      vecy = np.random.randn(m)
	      vecy /= np.linalg.norm(vecy, 2)

	      lbdmax = np.linalg.norm(matA.T @ vecy, np.inf)
	      lbd = .8 * lbdmax

	      # GapSphereTest(), LeDomeTest() 
	      list_screening = [None, GapSphereTest(), HolderDomeTest(), GapDomeTest()]
	      for screening in list_screening:
		      alg = Ista(matA, vecy, screening)

		      parameters = AlgParameters()
		      alg.solve(lbd, parameters)

		      self.assertTrue(alg.gap < parameters.gap_stop)


if __name__ == '__main__':
    unittest.main()