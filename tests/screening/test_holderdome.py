# -*- coding: utf-8 -*-
import unittest

import numpy as np
from src.utils.dictionaries import generate_dic

from src.screening.holderdome import HolderDomeTest
from src.screening.pregapsphere import PreGapSphereTest

from src.lasso.ista import Ista
from src.utils.algparameters import AlgParameters


class TestHolderDome(unittest.TestCase):

   def test_nonzero_does_not_pass(self):
      """ Run GAP test at "optimality"
         check if nonzero entry does no pass the test
      """

      # Create screening class
      screening = HolderDomeTest()
      screening.check_params = True

      # Lasso problem
      m = 50
      n = 80
      
      matA = generate_dic("gaussian", m, n)
      vecy = np.random.randn(m)
      vecy /= np.linalg.norm(vecy)

      lbdmax = np.linalg.norm(matA.T @ vecy, np.inf)
      lbd = .4 * lbdmax

      alg = Ista(matA, vecy)
      parameters = AlgParameters()
      xhat = alg.solve(lbd, parameters)

      Ax   = matA @ xhat
      vecu = vecy - Ax
      coef_dual_scaling = lbd / np.linalg.norm(matA.T @ vecu, np.inf)
      vecu *= coef_dual_scaling

      # Screening test
      # "y", "vecu", "y_minus_u", "normy_minus_u", "Ax", "normx1", "coef_dual_scaling", 
      # "At_gradf_x", "Aty", "AtAx"
      output_test, flop = screening.apply_test(
         lbd, 
         norm2y_minus_u    = np.linalg.norm(vecy - vecu, 2)**2,
         lbd_normx1        = lbd * np.linalg.norm(xhat, 1),
         normAx2           = np.linalg.norm(Ax, 2)**2,
         coef_dual_scaling = coef_dual_scaling,
         ytAx              = vecy.T @ Ax,
         At_gradf_x        = matA.T @ (Ax - vecy),
         Aty               = matA.T @ vecy,
         AtAx              = matA.T @ matA @ xhat,
      )

      ind_pass = np.where(output_test)[0]

      self.assertTrue(np.all(np.abs(xhat[ind_pass]) < 1e-8))


   def test_resume_to_sphere(self):
      """ Check if the test resume to some gap sphere test when
         the hyperplane is degenerated
      """

      # Create screening class
      screening = HolderDomeTest()
      screening.check_params = True

      screening_check = PreGapSphereTest()
      screening.check_params = True

      # Lasso problem
      np.random.seed(2401)
      m = 50
      n = 80
      
      matA = generate_dic("gaussian", m, n)
      vecy = np.random.randn(m)
      vecy /= np.linalg.norm(vecy)

      lbdmax = np.linalg.norm(matA.T @ vecy, np.inf)
      lbd = .8 * lbdmax

      xhat = np.zeros(n)
      Ax   = matA @ xhat
      vecu = vecy - Ax
      coef_dual_scaling = lbd / np.linalg.norm(matA.T @ vecu, np.inf)
      vecu *= coef_dual_scaling

      # Screening test
      output_test, flop = screening.apply_test(
         lbd, 
         norm2y_minus_u    = np.linalg.norm(vecy - vecu, 2)**2,
         lbd_normx1        = lbd * np.linalg.norm(xhat, 1),
         normAx2           = np.linalg.norm(Ax, 2)**2,
         coef_dual_scaling = coef_dual_scaling,
         ytAx              = vecy.T @ Ax,
         At_gradf_x        = matA.T @ (Ax - vecy),
         Aty               = matA.T @ vecy,
         AtAx              = matA.T @ matA @ xhat,
      )

      # "normy_minus_u", "coef_dual_scaling", "At_gradf_x", "Aty"
      output_check, flop = screening_check.apply_test(
         lbd=lbd, 
         normy_minus_u     = np.linalg.norm(vecy - vecu, 2),
         coef_dual_scaling = coef_dual_scaling,
         At_gradf_x        = matA.T @ (Ax - vecy),
         Aty               = matA.T @ vecy,
      )

      self.assertTrue(np.all(output_test == output_check))


if __name__ == '__main__':
    unittest.main()