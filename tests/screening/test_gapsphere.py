# -*- coding: utf-8 -*-
import unittest

import numpy as np
from src.utils.dictionaries import generate_dic

from src.screening.gapsphere import GapSphereTest

from src.lasso.ista import Ista
from src.utils.algparameters import AlgParameters


class TestASreening(unittest.TestCase):

   def test_nonzero_does_not_pass(self):
      """ Run GAP test at "optimality"
         check if nonzero entry does no pass the test
      """

      # Create screening class
      screening = GapSphereTest()
      screening.check_params = True

      # Lasso problem
      m = 50
      n = 80
      
      matA = generate_dic("gaussian", m, n)
      vecy = np.random.randn(m)
      vecy /= np.linalg.norm(vecy)

      lbdmax = np.linalg.norm(matA.T @ vecy, np.inf)
      lbd = .4 * lbdmax

      alg = Ista(matA, vecy)
      parameters = AlgParameters()
      xhat = alg.solve(lbd, parameters)

      Ax   = matA @ xhat
      vecu = vecy - Ax
      coef_dual_scaling = lbd / np.linalg.norm(matA.T @ vecu, np.inf)
      vecu *= coef_dual_scaling

      # Screening test
      pfunc = .5 * np.linalg.norm(vecy - Ax, 2)**2 + lbd * np.linalg.norm(xhat, 1)
      dfunc = .5 * (np.linalg.norm(vecy, 2)**2 - np.linalg.norm(vecy - vecu, 2)**2)
      gap = np.abs(pfunc - dfunc)

      output_test, flop = screening.apply_test(
         lbd, 
         gap               = gap,
         At_gradf_x        = matA.T @ (Ax - vecy),
         coef_dual_scaling = coef_dual_scaling
      )

      ind_pass = np.where(output_test)[0]

      self.assertTrue(np.all(np.abs(xhat[ind_pass]) < 1e-8))


if __name__ == '__main__':
    unittest.main()