# -*- coding: utf-8 -*-
import unittest

from src.utils.flop import Flop

class TestASreening(unittest.TestCase):

   def test_pass_by_reference(self):

      flop = Flop()

      def func(flop):
         flop.add_operation(5)

      func(flop)

      self.assertTrue(flop.nb_add == 5)

   def test_sum(self):

      flop1 = Flop()
      flop2 = Flop()

      flop1.add_operation(5)
      flop2.mult_operation(7)

      flop3 = flop1 + flop2

      self.assertTrue(flop3.nb_add == 5)
      self.assertTrue(flop3.nb_mult == 7)
      self.assertTrue(flop3.get_flops() == 12)

if __name__ == '__main__':
    unittest.main()