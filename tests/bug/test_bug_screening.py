# -*- coding: utf-8 -*-
import unittest

import numpy as np
from src.utils.dictionaries import generate_dic

from src.lasso.ista import Ista
from src.lasso.fista import Fista
from src.screening.gapsphere import GapSphereTest
from src.screening.gapdome import GapDomeTest
from src.screening.holderdome import HolderDomeTest
from src.utils.algparameters import AlgParameters


class TestBugXp(unittest.TestCase):

   def test_bug1_holder_dome(self):
      """ An issue with Holder Dome
          (was overly screening)
      """

      # Create screening class
      screening = HolderDomeTest()

      # Reproduce xp setup
      m = 100
      n = 300

      np.random.seed(9)
      
      matA = generate_dic("gaussian", m, n)
      vecy = np.random.randn(m)
      vecy /= np.linalg.norm(vecy)

      lbdmax = np.linalg.norm(matA.T @ vecy, np.inf)
      lbd = .8 * lbdmax

      # Screening test
      parameters = AlgParameters()

      parameters.iter_stop = np.inf
      parameters.gap_stop  = 1e-14
      parameters.flop_stop = np.inf

      alg = Ista(matA, vecy, HolderDomeTest())
      alg.solve(lbd, parameters)

      self.assertTrue(alg.gap <= parameters.gap_stop)


   def test_bug2_gap_calculation(self):
      """ The gap calculated by solver is different from the
          one calculated by hand
      """

      # Reproduce xp setup
      m = 100
      n = 300

      np.random.seed(240191)
      
      matA = generate_dic("gaussian", m, n)
      vecy = np.random.randn(m)
      vecy /= np.linalg.norm(vecy)

      lbdmax = np.linalg.norm(matA.T @ vecy, np.inf)
      lbd = .5 * lbdmax

      # Screening test
      parameters = AlgParameters()

      parameters.iter_stop = 10
      parameters.gap_stop  = 1e-14
      parameters.flop_stop = np.inf

      alg = Ista(matA, vecy)
      vecx = alg.solve(lbd, parameters)

      pfunc = .5 * np.linalg.norm(vecy - matA @ vecx, 2)**2
      pfunc += lbd * np.linalg.norm(vecx, 1)

      vecu  = vecy - matA @ vecx
      coeff = lbd / np.linalg.norm(matA.T @ vecu, np.inf)
      vecu *= coeff

      dfunc = .5 * np.linalg.norm(vecy, 2)**2
      dfunc -= .5 * np.linalg.norm(vecy - vecu, 2)**2

      self.assertTrue(
      	np.abs(alg.list_pfunc[-1] - pfunc) < 1e-14
      )

      self.assertTrue(
      	np.abs(alg.list_dfunc[-1] - dfunc) < 1e-14
      )


   def test_bug3_fista_safeSphere(self):
      """ On one instance, Fista + safe sphere was 
          unable to reach machine precision.
          In particular, Fista was unable to reach a gap lower that 1e-2
      """

      # Create screening class
      screening = GapSphereTest()

      # Reproduce xp setup
      m = 100
      n = 300

      np.random.seed(161)
      
      matA = generate_dic("uniform", m, n)
      vecy = np.random.randn(m)
      vecy /= np.linalg.norm(vecy)

      lbdmax = np.linalg.norm(matA.T @ vecy, np.inf)
      lbd = .5 * lbdmax

      # Screening test
      parameters = AlgParameters()

      parameters.iter_stop = 1e4
      parameters.gap_stop  = 1e-14
      parameters.flop_stop = np.inf

      alg = Fista(matA, vecy, screening)
      alg.solve(lbd, parameters)

      self.assertTrue(alg.gap <= parameters.gap_stop)


if __name__ == '__main__':
    unittest.main()