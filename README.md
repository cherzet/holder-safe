# Beyond GAP safe screening: Holder-safe toolbox

This repository contains numerical procedures to assess the efficiency and effectiveness of safe screening tests leveraging the family of dual cutting half-spaces introduced in [1].
In particular, reference [1] contains theoretical results and several applications that can be reproduced with this toolbox.

This python toolbox is currently under development and is hosted on Gitlab. If you encounter a bug or something unexpected please let me know by [raising an issue](https://gitlab.inria.fr/cherzet/holder_dome_lasso/-/issues) on the project page or contact the author by [mail](clement.elvira@centralesupelec.fr).

> [1] Thu-Le Tran, Clément Elvira, Hong-Phong. Dang, and Cédric Herzet, “Beyond GAP
screening for Lasso by exploiting new dual cutting half-spaces with supplementary material,” Technical report, 2022, available [online](https://c-elvira.github.io/pdf/tecreports/Le2022TR.pdf).

# Running the code

## Requirements

Holder-safe have been developed with python 3.8 and should work with python 3.6+.

Main dependencies:
 -   [NumPy](http://www.numpy.org)
 -   [SciPy](https://www.scipy.org)
 -   [Matplotlib](http://matplotlib.org)


## Install from sources

1. Clone the repository
```bash
git clone https://gitlab.inria.fr/cherzet/holder_dome_lasso.git
```

2. Enter the folder
```bash
cd holder_dome_lasso
```

3. (Optional) Create a virtual environment and activate it
```bash
virtualenv venv -p python3
source venv/bin/activate
```

4. Install the dependencies
```bash
pip install -r requirements.txt
```

5. And execute `setup.py`
```bash
pip install .
```
or 
```bash
pip install -e .
```
if you want the code editable.

To check if the code is running correctly, you can e.g. execute all unit-tests:
```bash
./run_unittest.sh
```
To do so, you may need to make need to make the script executable (`chmod a+x run_unittest.sh` in a Shell).


## Running the experiments

Seeds used to obtain the results in [1] are provided in this package.
To reprodure the figures from the paper, follow the following set of commands:
```bash
cd xps/Eusipco/xxx
./launcher
```
where xxx is either `xp1_radii` or `xp2_benchmark`.
Run then
```bash
python python viz_eusipco.py
```
to visualize the result or
```bash
python python viz_eusipco.py --save
```
if you want to save the plots.

A convenient way to design new simulation setups is to modify the `setups.json` file provided with each experiment.
To execute then, simply adapt the launcher that is provided `launcher.sh` by changing the experiment id.


# License

This software is distributed under the [GNU AGPLv3 License Agreement](https://www.gnu.org/licenses/agpl-3.0.en.html)


# Cite this work

If you use this package for your own work, please consider citing it with this piece of BibTeX:

```bibtex
@techreport{Le2022_HolderDomeTechreport,
	author = {Tran, Thu-Le and Elvira, Cl{\'e}ment and Dang, Hong-Phuong and Herzet, C{\'e}dric},
	title = {Beyond {GAP} screening for {Lasso} by exploiting new dual cutting half-spaces with supplementary material},
	year = {2022},
	note = {available at \url{https://c-elvira.github.io/pdf/tecreports/Le2022TR.pdf}},
}
```